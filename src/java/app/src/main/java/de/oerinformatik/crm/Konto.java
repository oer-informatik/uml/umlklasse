package de.oerinformatik.crm;

import java.util.ArrayList;

interface Zahlbar{
    public abstract boolean auszahlen(double betrag);
    public abstract boolean einzahlen(double betrag);
}

public class Konto extends Geldanlage implements Zahlbar{
    private String iban;
    protected double kontostand;
    private static ArrayList<Konto> kontenListe = new ArrayList<>();
    
    public String getIban() {
        return iban;
      }
    
    public void setIban(String iban) {
        this.iban = iban;
    }

    public boolean einzahlen(double betrag) {
      kontostand = kontostand + betrag;
      return true;
    }

    public boolean auszahlen(double betrag) {
        if (kontostand - betrag > 0){
            kontostand = kontostand - betrag;
            return true;
        }else{
            return false;
        }
    }
    
    static String listeStatusAllerKonten(){
        String ausgabe ="";
        for (Konto einKonto: kontenListe){
            ausgabe = ausgabe + "\n" + einKonto.toString();
        }
        return ausgabe;
    }
    
    public String toString(){
        return "KontoNr "+this.iban+" Kontostand: "+Double.toString(this.kontostand);
    }

    Konto(String iban){
        super("Konto "+iban);
        this.iban = iban;
        kontostand =0.0;
        kontenListe.add(this);
    }

    Konto(String ktn, String blz){
        super("Konto "+blz+ktn);
        this.iban = "DE__"+blz+ktn;
        kontenListe.add(this);
    }

    public double getKontostand() {
        return kontostand;
    }

    public double getAnlagewert() {
        return getKontostand();
    }
}

  class PrivatKonto extends Konto{
    private String vorname;
    private String nachname;

    public PrivatKonto(String iban, String vorname, String nachname){
        super(iban);
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public String getName(){
      return this.vorname + " " + this.nachname;
    }   
}

class GeschaeftsKonto extends Konto {

    private String firmenname;

    private double verfuegungsrahmen;

    public double getVerfuegungsrahmen() {
        return verfuegungsrahmen;
    }

    public void setVerfuegungsrahmen(double verfuegungsrahmen) {
        this.verfuegungsrahmen = verfuegungsrahmen;
    }

    public GeschaeftsKonto(String iban, String firmenname, double verfuegungsrahmen) {
        super(iban);
        this.firmenname = firmenname;
        this.setVerfuegungsrahmen(verfuegungsrahmen);
    }

    @Override
    public boolean auszahlen(double betrag) {
      if (betrag > verfuegungsrahmen) {
          return false;
      } else {
          return super.auszahlen(betrag);
      }  
    }  
  }

class SparKonto extends PrivatKonto implements Verzinsbar{
    private double zinssatz;
    public double getZinssatz() {return zinssatz;}
    public void setZinssatz(double zinssatz) {this.zinssatz = zinssatz;}

    public SparKonto(String iban , String vorname, String nachname, double zins ){
        super(iban, vorname, nachname);
        this.setZinssatz(zins);
    }

    public void verzinse(){
        this.kontostand = this.kontostand * (1+zinssatz);
    }  
}

interface Verzinsbar{
    public abstract void verzinse();
}

