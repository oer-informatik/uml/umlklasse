class Rechnung:
    def __init__(self, rechnungsbetrag: float):
        self.__rechnungsbetrag: float
        self.__rechnungsbetrag = rechnungsbetrag
        self.__bezahlt: bool
        self.__bezahlt = False

    def get_rechnungsbetrag(self):
        return self.__rechnungsbetrag;
    
    def set_bezahlt(self, bezahlt: bool, kunde):
        self.__bezahlt = bezahlt
        print("wurde gezahlt von "+str(kunde.get_name()))

class Kunde:
    def __init__(self, vermoegen:float, name:str):
        self.__vermoegen: int
        self.__vermoegen = vermoegen
        self.__name = name
        
    def bezahle_rechnung(self, rechnung):
        self.__vermoegen -= rechnung.get_rechnungsbetrag()
        rechnung.set_bezahlt(True, self)

    def get_name(self) -> str:
        return self.__name

meine_rechnung = Rechnung(rechnungsbetrag=100)
mein_kunde = Kunde(vermoegen=1000, name = "Jeff Bezos")
mein_kunde.bezahle_rechnung(meine_rechnung)
print(mein_kunde._Kunde__vermoegen)
