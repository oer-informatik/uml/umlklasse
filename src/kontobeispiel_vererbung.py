
class Konto:
    kontenliste = []

    def __init__(self, iban: str) -> None:
        self.__kontostand: float 
        self.__kontostand = 0
        self.__iban: str 
        self.__iban = iban
        Konto.kontenliste.append(self)

    @property
    def iban(self) ->str:
        return self.__iban

    @iban.setter
    def iban(self, iban) ->None:
        self.__iban = iban
    
    @property
    def kontostand(self) -> float:
        return self.__kontostand

    @kontostand.setter
    def kontostand(self, betrag):
        self.__kontostand = betrag
        
    def einzahlen(self, betrag: float) -> None:
        self.__kontostand += betrag

    def auszahlen(self, betrag: float) -> bool:
        if (kontostand-betrag) >= 0:
            self.__kontostand -= betrag
            return True
        else:
            return False

    @staticmethod
    def liste_status_aller_konten():
        ausgabe = "Alle Konten:\n"
        for konto in Konto.kontenliste:
            ausgabe += str(konto)+"\n"
        return ausgabe

    def __str__(self):
        return 'Der Kontostand von {0} ist {1}. (Objekt: {2})'.format(self.iban, self.kontostand, self.__repr__())+"\n"

class Privatkonto(Konto):
    def __init__(self, iban, vorname: str, nachname: str) -> None:
        super().__init__(iban)
        self.__vorname: str 
        self.__vorname = vorname
        self.__nachname: str 
        self.__nachname = nachname
    
    @property
    def privatkundenname(self) ->str:
        return self.__vorname + " " + self.__nachname

    def __str__(self):
        return 'Privatkonto von '+self.__vorname + " " + self.__nachname+"\n"+super().__str__()

class Geschaeftskonto(Konto):
    def __init__(self, iban, firmenname: str, verfuegungsrahmen: float) -> None:
        super().__init__(iban)
        self.__firmenname: str 
        self.__firmenname = firmenname
        self.__verfuegungsrahmen: float
        self.__verfuegungsrahmen = verfuegungsrahmen
    
    @property
    def verfuegungsrahmen(self) ->float:
        return self.__verfuegungsrahmen
    
    @verfuegungsrahmen.setter
    def verfuegungsrahmen(self, verfuegungsrahmen):
        self.__verfuegungsrahmen = verfuegungsrahmen

    def auszahlen(self, betrag: float) -> bool:
        if (betrag) > self.__verfuegungsrahmen:
            return False
        else:
            return super().auszahlen(betrag)

    @property
    def firmenname(self) ->str:
        return self.__firmenname

    def __str__(self):
        return 'Geschäftskonto von '+self.__firmenname+"\n"+super().__str__()

class Sparkonto(Privatkonto):
    def __init__(self, iban, vorname: str, nachname: str, startzins: float) -> None:
        super().__init__(iban, vorname, nachname)
        self.__zinssatz: float 
        self.__zinssatz = startzins
    
    @property
    def zinssatz(self) ->float:
        return self.__zinssatz

    @zinssatz.setter
    def zinssatz(self, zinssatz):
        self.__zinssatz = zinssatz

    def verzinse(self):
        self.kontostand = self.kontostand * (1+self.zinssatz) 

    def __str__(self):
        return 'Sparkonto mit einem Zins von '+str(self.__zinssatz)+"\n"+super().__str__()

ein_konto = Konto("DE123")
ein_konto.einzahlen(100)
print("Der Kontostand von {0} ist {1}.".format(ein_konto.iban, ein_konto.kontostand))

ein_privatkonto = Privatkonto("DE345", "Martin", "Mustermann")
ein_privatkonto.einzahlen(200)
print("Der Kontostand von {0} ({1}) ist {2}.".format(ein_privatkonto.privatkundenname, ein_privatkonto.iban, ein_privatkonto.kontostand))

ein_geschaeftskonto = Geschaeftskonto("DE345", "Muster GmbH", 0)
ein_geschaeftskonto.einzahlen(200)
ein_geschaeftskonto.verfuegungsrahmen =150
print("Ausgezahlt: "+str(ein_geschaeftskonto.auszahlen(200)))
print("Der Kontostand von {0} ({1}) ist {2}.".format(ein_geschaeftskonto.firmenname, ein_geschaeftskonto.iban, ein_geschaeftskonto.kontostand))

ein_sparkonto = Sparkonto("DE345", "Martin", "Mustermann", 0.03)
ein_sparkonto.einzahlen(200)
ein_sparkonto.verzinse()
print("Der Kontostand von {0} ({1}) ist {2}.".format(ein_sparkonto.privatkundenname, ein_sparkonto.iban, ein_sparkonto.kontostand))

print(ein_sparkonto.liste_status_aller_konten())