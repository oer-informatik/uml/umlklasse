package de.oerinformatik.crm;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import de.oerinformatik.abrechnung.Buchungen;

public class Investmentrechner {
    public static void main(String[] args) {
        Konto hannesKonto = new Konto("DE123");
        Konto tobiasKonto = new Konto("456", "10010010");
        
        Buchungen meineBuchungen = new Buchungen();
        meineBuchungen.setKonto(tobiasKonto);

        System.out.println(Konto.listeStatusAllerKonten());

        Kunde kundeMustermann = new Kunde("Max", "Mustermann");
        Rechnung versandRechnung = new Rechnung();
        versandRechnung.addRechnungsposition("Festplatte WDRedLabel10", "12345", 2, 123.45);
        versandRechnung.addRechnungsposition("Cherry Tastatur", "54321", 1, 54.32);
        versandRechnung.setRechnungsbetrag(100.0);
        kundeMustermann.bezahleRechnung(versandRechnung);
        System.out.println(versandRechnung);

        showAbstraction();
    }

    public static void showAbstraction(){
        List<Geldanlage> meineInvestments = new ArrayList<>();
        Aktie investment1 = new Aktie("Telekom");
        investment1.setKurswert(100);
        investment1.kaufeAktie(1);
        meineInvestments.add(investment1);

        Konto investment2 = new Konto("DE123");
        investment2.einzahlen(200);
        meineInvestments.add(investment2);

        Festgeld investment3 = new Festgeld(1000,LocalDate.now().minusYears(5), LocalDate.of(2035,12,31), 0.04);
        meineInvestments.add(investment3);

        for(Geldanlage i : meineInvestments){
            System.out.println("Investment "+i.getName()+" Guthaben: "+Double.toString(i.getAnlagewert()));
        }

        SparKonto meinSparbuch = new SparKonto("DE123", "Max", "Mustermann",0.03); 
        System.out.println("Kontostand vor der Verzinsung: " + Double.toString(meinSparbuch.getKontostand()));
        meinSparbuch.verzinse();
        System.out.println("Kontostand nach der Verzinsung: " + Double.toString(meinSparbuch.getKontostand()));

        Kunde kundeMustermann = new Kunde("Max", "Mustermann");
        Rechnung versandRechnung = new Rechnung();
        versandRechnung.addRechnungsposition("Festplatte WDRedLabel10", "12345", 2, 123.45);
        versandRechnung.addRechnungsposition("Cherry Tastatur", "54321", 1, 54.32);
        versandRechnung.setRechnungsbetrag(100.0);
        
        kundeMustermann.bezahleRechnung(versandRechnung, meinSparbuch);
        System.out.println(versandRechnung);
    
    }
}
