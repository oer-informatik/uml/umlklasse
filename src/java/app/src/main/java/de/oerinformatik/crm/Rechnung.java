package de.oerinformatik.crm;

import java.util.ArrayList;

public class Rechnung {
    private double rechnungsbetrag;
    private boolean bezahlt;
    private String bezahltVon = "";

    public double getRechnungsbetrag(){
        return rechnungsbetrag;
    }

    public void setRechnungsbetrag(double rechnungsbetrag){
        this.rechnungsbetrag = rechnungsbetrag;
    }

    public void setBezahlt(boolean bezahlt, Kunde kunde) {
        this.bezahlt = bezahlt;
        bezahltVon = kunde.toString();
        System.out.println("wurde gezahlt von "+bezahltVon);
    } 

   private ArrayList<Rechnungsposition> rechnungspositionen = new ArrayList<Rechnungsposition>();

   public void addRechnungsposition(String name, String artikelnummer, int anzahl, double nettopreis) {
       rechnungspositionen.add(new Rechnungsposition(name, artikelnummer, anzahl, nettopreis));
   }

   public Rechnungsposition removeRechnungsposition(int index) {
    return rechnungspositionen.remove(index);
    }

   @Override
   public String toString() {
       String text = "Rechnung: \n";
       for (Rechnungsposition position : rechnungspositionen) {
           text += rechnungspositionen.toString() + "\n";
       }
       if (this.bezahlt){
        text += "Rechnung wurde gezahlt von "+bezahltVon+ "\n";
       }else{
        text += "Rechnung ist noch offen"+ "\n";
       }
       return text;
    } 
}

class Rechnungsposition {

    private String name;
    private String artikelnummer;
    private int anzahl;
    private double nettopreis;

    Rechnungsposition(String name, String artikelnummer, int anzahl, double nettopreis) {        this.name = name;
        this.artikelnummer = artikelnummer;
        this.anzahl = anzahl;
        this.nettopreis = nettopreis;
    }
 
    @Override
    public String toString() {
        return "Name: " + this.name + " /  Artikelnummer: " + artikelnummer + " / Anzahl: " + this.anzahl + " / Nettoeinzelpreis: " + this.nettopreis;
    }    
}
