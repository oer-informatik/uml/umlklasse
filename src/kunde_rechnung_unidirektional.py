class Rechnung:
    def __init__(self, rechnungsbetrag: float):
        self.__rechnungsbetrag: float
        self.__rechnungsbetrag = rechnungsbetrag
        self.__bezahlt: bool
        self.__bezahlt = False

    def get_rechnungsbetrag(self):
        return self.__rechnungsbetrag;
    
    def set_bezahlt(self, bezahlt: bool):
        self.__bezahlt = bezahlt

class Kunde:
    def __init__(self, vermoegen:float ):
        self.__vermoegen: int
        self.__vermoegen = vermoegen
        
    def bezahle_rechnung(self, rechnung):
        self.__vermoegen -= rechnung.get_rechnungsbetrag()
        rechnung.set_bezahlt(True)

meine_rechnung = Rechnung(rechnungsbetrag=100)
mein_kunde = Kunde(vermoegen=1000)
mein_kunde.bezahle_rechnung(meine_rechnung)
print(mein_kunde._Kunde__vermoegen)
