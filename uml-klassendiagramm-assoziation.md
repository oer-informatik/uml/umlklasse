## UML-Klassendiagramm Objektbeziehungen - Assoziationen

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110425552692026065</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-klassendiagramm-assoziation</span>


> **tl/dr;** _(ca. 10 min Lesezeit): Mit einzelnen Objekten erreicht man in der objektorientierten Programmierung (OOP) noch nicht viel. Erst im Zusammenspiel als Objektsammlungen und über Objektinteraktionen entwickelt die OOP ihre Stärken. Der folgende Text behandelt die Fragen: Welche Objektbeziehungen gibt es? Was unterscheidet Assoziation, Aggregation und Komposition? Versuch der Klärung._

Dieser Text ist ein Teil der Infotext-Serie zu UML-Klassendiagrammen:

* [Grundlagen UML-Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm)

* [Sichtbarkeitsmodifikatoren](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)

* [Objektbeziehungen (Assoziation/Aggregation/Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation)

* [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung)

* [abstrakte Klassen und Interfaces](https://oer-informatik.de/uml-klassendiagramm-interface-abstraktion)

* [Polymorphie in der objektorientierten Programmierung](https://oer-informatik.de/uml-klassendiagramm-polymorphie)

* [Objektinstanzen im UML-Objektdiagramm darstellen](https://oer-informatik.de/uml-objektdiagramm)

* [UML-Klassendiagramme mit PlantUML erstellen](https://oer-informatik.de/uml-klassendiagramm-plantuml)

Im Gegensatz zur Vererbung, die Beziehungen zwischen Klassen darstellt, beschreiben Assoziationen Beziehungen zwischen Objekten. Objektbeziehungen können zur Laufzeit eines Programms geändert werden.

### Assoziation

Beziehungen zwischen Objekten nennt man Assoziation, wenn ein Objekt eine Methode eines anderen Objekts aufruft (oder ein Attribut eines anderen Objekts ändert). Beide Objekte arbeiten für ein gemeinsames Ziel zusammen. Die Beziehungen werden im UML Klassendiagramm durch eine Linie zwischen den Objekten dargestellt:

![Assoziation zwischen Kunde und Rechnung](plantuml/05-Klassendiagramm-Assoziation.png)

Das gemeinsame Ziel der beteiligten Objekte wird oft anhand des Assoziationsnamens klar. Phrasen wie "benutzt ein", "ist zugeordnet zu" oder "hat eine Beziehung zu" können diese Assoziationen recht unkonkret beschreiben, häufig finden sich präzisere Namen der Beziehung. Diese Assoziationsnamen werden auf der Assoziationskante mit Pfeil in Leserichtung angegeben:
_ein/e Kund\*in bezahlt eine Rechnung_.

![Assoziation: ein Kunde bezahlt eine Rechnung](plantuml/05-Klassendiagramm-Assoziation-Name.png)

In einer Assoziation wird aber keine über das eigentliche Ziel hinaus gehende Abhängigkeit beschrieben. Wenn die Rechnung bezahlt ist, haben Kund*in und Rechnung also ggf. keine weitere Beziehung zueinander. Anhand einer konkreteren Darstellung soll die temporäre Zusammenarbeit beider Objekte deutlich werden:

![Eine Kund*in bezahlt eine Rechnung: konkretere Darstellung](plantuml/05-Klassendiagramm-Assoziation-Detail.png)

Das Ziel der Zusammenarbeit ist das Bezahlen einer Rechnung. Dazu erhält ein Kundenobjekt bei Aufruf der Methode `bezahleRechnung()` ein Rechnungsobjekt übergeben. Die Kund*in nutzt die `Rechnung`-Methoden `getRechnungsbetrag()` und `setBezahlt()`, um die Rechnung von seinem `vermoegen` zu bezahlen. Nach Abschluss dieser Transaktion hält der `Kunde` keine Referenz mehr auf die `Rechnung` - beide Objekte können unabhängig voneinander fortbestehen.

#### Gerichtete Assoziation

Bei den bisherigen Darstellungen wurde im UML-Diagramm offengelassen, ob beide Objekte einander _kennen_. Beide Enden der Assoziationskante waren unspezifiziert.

Wenn genauer festgelegt werden soll, welches Objekt vom anderen _weiß_ - also Zugriff auf eine Referenz hat - kann dies mit Kantenenden spezifisch dargestellt werden:

#### Unidirektional navigierbare Assoziationen

![unidirektionale Navigation von Kunde zu Rechnung](plantuml/05-Klassendiagramm-Assoziation-Navigationsrichtung.png)

Ein `Kunde` kann auf die `Rechnung` zugreifen (Pfeil in Richtung `Rechnung`), eine Rechnung _weiß_ jedoch nicht, welche Kund*in sie bezahlt hat (Kreuz auf der Kantenseite `Kunde`). Eine `Rechnung` hält also keine Referenz auf `Kunde`. Das wird v.a. auch im Beispielcode deutlich:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
package de.oerinformatik.crm;

public class Rechnung {
    private double rechnungsbetrag;
    private boolean bezahlt;

    public double getRechnungsbetrag(){
        return rechnungsbetrag;
    }

    public void setRechnungsbetrag(double rechnungsbetrag){
        this.rechnungsbetrag = rechnungsbetrag;
    }
    
    public void setBezahlt(boolean istBezahlt){
        bezahlt = istBezahlt;
    }
}
```

```java
package de.oerinformatik.crm;

public class Kunde {
    private double vermoegen;

    public void bezahleRechnung(Rechnung rechnung){
        vermoegen -= rechnung.getRechnungsbetrag();
        rechnung.setBezahlt(true);
    }
    Kunde(double vermoegen){
        this.vermoegen = vermoegen;
    }
}
```
</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Rechnung:
    def __init__(self, rechnungsbetrag: float):
        self.__rechnungsbetrag: float
        self.__rechnungsbetrag = rechnungsbetrag
        self.__bezahlt: bool
        self.__bezahlt = False

    def get_rechnungsbetrag(self):
        return self.__rechnungsbetrag;
    
    def set_bezahlt(self, bezahlt: bool):
        self.__bezahlt = bezahlt

class Kunde:
    def __init__(self, vermoegen:float ):
        self.__vermoegen: int
        self.__vermoegen = vermoegen
        
    def bezahle_rechnung(self, rechnung):
        self.__vermoegen -= rechnung.get_rechnungsbetrag()
        rechnung.set_bezahlt(True)
```

[Link zum Python-Code](https://gitlab.com/oer-informatik/uml/umlklasse/-/raw/main/src/kunde_rechnung_unidirektional.py)

</span>


Da diese Assoziation nur in _einer_ Richtung navigiert werden kann, spricht man von einer _unidirektional navigierbaren Assoziation_.

#### Bidirektional navigierbare Assoziation

Wird beispielsweise beim Bezahlvorgang aufseiten der `Rechnung` vermerkt, welche Kund*in die Rechnung bezahlt hat (z.B. in der Methode `setBezahlt(istBezahlt:boolean, kunde:Kunde)`), so _kennen_ sich beide Objekte. Die Assoziation kann in beide Richtungen navigiert werden (`Kunde` ruft Methoden von `Rechnung` auf und umgekehrt). Wir sprechen dann von einer _bidirektionale navigierbaren Assoziation_. In der UML wird dies durch zwei Pfeilenden an der Assoziationskante gekennzeichnet:

![Kunde kennt Rechnung, Rechnung kennt Kunde](plantuml/05-Klassendiagramm-Assoziation-Navigationsrichtung-bidirektional.png)

Im Beispielcode wird der gegenseitige Methodenaufruf der beiden Klassen deutlich:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
public class Rechnung {
    private double rechnungsbetrag;
    public double getRechnungsbetrag() {return rechnungsbetrag;}

    private boolean bezahlt=false;
    public void setBezahlt(boolean bezahlt, Kunde kunde) {
        this.bezahlt = bezahlt;
        System.out.println("wurde gezahlt von "+kunde.toString());
}   }

public class Kunde {
    private int vermoegen;
    public void bezahleRechnung(Rechnung rechnung){
        vermoegen -= rechnung.getRechnungsbetrag();
        rechnung.setBezahlt(true, this);
    }   }
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Rechnung:
    def __init__(self, rechnungsbetrag: float):
        self.__rechnungsbetrag: float
        self.__rechnungsbetrag = rechnungsbetrag
        self.__bezahlt: bool
        self.__bezahlt = False

    def get_rechnungsbetrag(self):
        return self.__rechnungsbetrag;
    
    def set_bezahlt(self, bezahlt: bool, kunde):
        self.__bezahlt = bezahlt
        print("wurde gezahlt von "+str(kunde.get_name()))

class Kunde:
    def __init__(self, vermoegen:float, name:str):
        self.__vermoegen: int
        self.__vermoegen = vermoegen
        self.__name = name
        
    def bezahle_rechnung(self, rechnung):
        self.__vermoegen -= rechnung.get_rechnungsbetrag()
        rechnung.set_bezahlt(True, self)

    def get_name(self) -> str:
        return self.__name
```

[Link zum Python-Code](https://gitlab.com/oer-informatik/uml/umlklasse/-/raw/main/src/kunde_rechnung_bidirektional.py)

</span>

#### Nicht navigierbare Assoziation

Modellierbar ist auch der Fall, dass keine Richtung navigierbar ist. Keines der Objekte hält dann direkt Referenzen auf das jeweils andere (zwei "x" an den Kantenenden), es besteht nur eine logische, aber keine faktische Beziehung. Ich bin für jeden Hinweis dankbar, wann es einen sinnvollen Anwendungsfall hierfür gibt und wie dieser implementiert werden muss.

### Multiplizitäten

Bei den bisherigen Notationsmitteln für Assoziationen wurde noch keine Festlegung getroffen, wie viele Objekte der einen Klasse mit wie vielen Objekte der anderen Klasse in Beziehung stehen. Im folgenden Diagramm sind eine Reihe von Festlegungen für ein Programm zur Erstellung von Rechnungen getroffen. Zu jeder Kante können zwei unterschiedliche Multiplizitäten ausgewiesen werden: für jeder der beiden Klassen je eine:

![Rechnung, Rechnungsanschrift, Lieferanschrift, Rechnungsposition und Steuersatz](plantuml/05-Klassendiagramm-Assoziation-Multiplizitäten.png)

Sofern die Multiplizität die Zahl 1 überschreitet, werden die entsprechenden Referenzen in Objektsammlungen gespeichert. Je nach Programmiersprache können diese als `Array`, `ArrayList`, `List` o.ä. umgesetzt werden. Jede Multiplizität legt fest, wie viele Instanzen der Klasse, an der sie notiert ist, an der Beziehung mindestens und höchstens beteiligt sind. Möglich sind folgende Notationen:

| UML-Notation<br/> am Kantenende | Bedeutung | Beispiel|
|---|---|---|
|`n` oder <br/>`n..n`<br/><br/>(`n` ist positive Ganzzahl)|feste Anzahl:<br/>Beziehung zu genau `n` Instanzen| Auf jeder Rechnung wird _genau eine_ Lieferanschrift genannt; Jede Rechnungsposition ist _genau einer_ Rechnung zugeordnet. Da hier Unter- und Obergrenze identisch sind (z.B. `3..3`) kann eine Grenze weggelassen werden (`3`). |
|`0..1`|optional:<br/>Beziehung zu keiner oder einer Instanz|Eine Rechnungsanschrift ist _optional_. Eine Rechnung kann eine gesonderte Rechnungsanschrift haben (andernfalls ist sie identisch mit der Lieferanschrift).|
|`m..n`<br/><br/>(`n` und `m` sind Ganzzahlen, `0 <= m < n`)|Beziehungen zwischen `m` und `n` unterschiedlichen Instanzen|Es gibt genau zwei Steuersätze (ermäßigt/normal). Auf jeder Rechung wird mindestens ein Steuersatz ausgewiesen, höchstens aber beide.|
|`*` oder <br/>`0..*`|null oder mehr<br/>Beziehung zu beliebig vielen Instanzen (oder zu keiner)|Ein Steuersatz kann auf beliebig vielen Rechnungen ausgewiesen sein. Denkbar auch, dass er auf keiner Rechnung ausgewiesen wird. Anstelle von `0..*` kann auch verkürzt `*` notiert werden.|
|`n..*`<br/><br/>(`n` ist positive Ganzzahl)|mindestens `n` oder mehr|Auf jeder Rechnung findet sich _mindestens eine_ Rechnungsposition.|

Im Programmtext muss anhand des Datentyps der Referenz oder über Implementierung sichergestellt werden, dass die jeweilige Anzahl an Instanzen übergeben werden kann.

#### Die Begriffe _Kardinalität_ und _Multiplizität_

In der UML wird eine tatsächliche Anzahl der verknüpften Instanzen _Kardinaltät_ genannt. An folgendem Beispiel soll das deutlich werden: Ein Fahrzeug kann eines oder viele Räder haben.

![Rechnung, Rechnungsanschrift, Lieferanschrift, Rechnungsposition und Steuersatz](plantuml/05-Klassendiagramm-Assoziation-Multiplizität-Kardinalität.png)

Ist das Fahrzeug ein Fahrrad, so beträgt die tatsächliche Anzahl (die Kardinalität) _2_, bei einem Auto _4_, bei einem Einrad _1_, bei einer Ape _3_ und bei einem Zug ein Vielfaches von 4 - z.B. _128_. Der Begriff _Kardinalität_ beschreibt in der UML also die konkrete Anzahl an Beziehungen einer identifizierten Instanz der Klasse. Das weicht von der Bedeutung ab, mit der Kardinalität bei der Datenbankmodellierung (_Entity Relationship Model_) verwendet wird - dort ist Kardinalität synonym zu Multiplizität zu sehen.

In der UML ist der Begriff Multiplizität definiert als die Menge aller zulässigen Kardinalitäten. Ein Fahrzeug allgemein kann ein oder viele Räder haben: Die Multiplizität ist daher `1..*`.

### Aggregation

Die bisherigen Assoziationen beschreiben eine temporäre, einem Ziel zugeordnete Zusammenarbeit zweier Objekte. Wir können diese mit "benutzt ein" beschreiben. Eine engere Bindung zweiter Objekte ist gegeben, wenn Objekte zueinander eine dauerhafte Teil-Ganzes-Beziehung haben, die mit Phrasen wie "besteht aus" oder "hat ein/e" beschrieben werden können (in umgekehrter Leserichtung: "ist Teil von").

Die zugeordneten Instanzen der Teile werden i.d.R. in Attributen des Ganzen verwaltet. Per Setter-Methoden wird das Ganze (das _Aggregat_) zusammengesetzt, somit im wörtlichen Sinne aggregiert. Man nennt diese Beziehung daher Aggregation.

Im UML-Klassendiagramm wird diese besondere Assoziation durch eine nicht ausgefüllte Raute aufseiten des Ganzen notiert.

![Ein Konto ist Teil des Objekts Kunde](plantuml/05-Klassendiagramm-Aggregation.png)

Bei einer Aggregation kann das Teil auch unabhängig vom Ganzen existieren - es gibt Referenzen auf das Teil außerhalb des Ganzen (z.B. in dem die Teil-Instanz außerhalb des Ganzen erzeugt wird).

Es kann auch Teilinstanzen geben, die mehreren Ganzen zugeordnet sind. Denkbar wäre im obigen Beispiel etwa, dass Konten mehreren Kund*innen zugeordnet werden oder eine Kund*in gelöscht wird, die Konten aber weiterbestehen. Ein Quelltextbeispiel:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
public class Kunde {
    private ArrayList<Konto> kontenListe;
    public void addKonto(Konto konto){this.kontenListe.add(konto);}

    public double gesamtVermoegen(){
        double vermoegen = 0;
        for(Konto konto:kontenListe){
            vermoegen+=konto.getKontostand();
        }
        return vermoegen;
}   }

public class Konto {
    private double kontostand;
    public double getKontostand() {return kontostand;}
}
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Kunde:
    def __init__(self):
        self.__kontenliste = []

    def add_konto(self, konto):
        self.__kontenliste.append(konto)

    def gesamt_vermoegen(self):
        vermoegen = 0.0
        for konto in self.__kontenliste:
            vermoegen += konto.get_kontostand()
        return vermoegen
    

class Konto:
    def __init__(self, kontostand: float):
        self.__kontostand: float
        self.__kontostand = kontostand

    def get_kontostand(self):
        return self.__kontostand
    
    def set_bezahlt(self, bezahlt: bool, kunde):
        self.__bezahlt = bezahlt
        print("wurde gezahlt von "+str(kunde.get_name()))
```

[Link zum Python-Code](https://gitlab.com/oer-informatik/uml/umlklasse/-/raw/main/src/kunde_rechnung_bidirektional.py)

</span>

### Komposition

Bei der Komposition handelt es sich um eine Sonderform der Teil/Ganzes-Beziehung: Die Teilobjekte existieren nur innerhalb des Ganzen und sind somit in Ihrer Lebensdauer auf die Lebensdauer des zugehörigen Ganzen beschränkt. Da dies eine festere Bindung zwischen Teil und Ganzem ist, wird in der Literatur hierfür häufig auch der Begriff "starke Aggregation" gewählt.

Aufgrund dieser Abhängigkeit kann ein Teil auch nur mit einem Ganzen in Beziehung stehen - die Multiplizität aufseiten des Ganzen ist per Definition in der Regel "1" (der Defaultwert) wird daher oft weggelassen. 

Die Komposition wird mir einer ausgefüllten Raute aufseiten des Ganzen gekennzeichnet:

![Eine Rechnungsposition ist existenziell abhängig von einer Rechnung](plantuml/05-Klassendiagramm-Komposition.png)

Kompositionen erkennt man in der Programmierung in der Regel daran, dass das Teil innerhalb des Ganzen erzeugt wird und das Teilobjekt nicht über _Getter_ oder öffentliche Attribute nach außen veröffentlicht wird. Nur das Ganze hält Referenzen auf das Teil.

Die Entscheidung, ob eine Beziehung als Aggregation oder Komposition umgesetzt wird, trifft der/die Programmierer*in. Häufig gibt die Fachdomäne über gegebene Abhängigkeiten bereits Hinweise, was sinnvoll ist. In der Regel führt die Komposition zu besser wartbarem Code, da weniger externe Abhängigkeiten möglich sind. 

Quelltextbeispiel:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

 ```java
 public class Rechnung {

    private ArrayList<Rechnungsposition> rechnungspositionen = new ArrayList<Rechnungsposition>();

    public void addRechnungsposition(String name, String artikelnummer, Integer anzahl, double nettopreis) {
        rechnungspositionen.add(new Rechnungsposition(name, artikelnummer, anzahl, nettopreis));
    }

    @Override
    public String toString() {
        String text = "Rechnung: \n";
        for (Rechnungsposition position : rechnungspositionen) {
            text += rechnungspositionen.toString() + "\n";
        }
        return text;
}   }


class Rechnungsposition {

    private String name;
    private String artikelnummer;
    private Integer anzahl;
    private double nettopreis;

    Rechnungsposition(String name, String artikelnummer, Integer anzahl, double nettopreis) {
        this.name = name;
        this.artikelnummer = artikelnummer;
        this.anzahl = anzahl;
        this.nettopreis = nettopreis;
    }

    @Override
    public String toString() {
        return "Name: " + this.name + " /  Artikelnummer: " + artikelnummer + " / Anzahl: " + this.anzahl + " / Nettoeinzelpreis: " + this.nettopreis;
}    }
 ```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
 class Rechnung:
    def __init__(self):
        self.__rechnungspositionen = []

    def add_rechnungsposition(self, name:str, artikelnummer:str, anzahl: int, nettopreis:float):
        rechnungsposition = Rechnungsposition(name, artikelnummer, anzahl, nettopreis)
        self.__rechnungspositionen.append(rechnungsposition)


    def __str__(self):
        text = ""
        for position in self.__rechnungspositionen:
            text += str(position) + "\n"
        return text
    
class Rechnungsposition:
    def __init__(self, name:str, artikelnummer:str, anzahl: int, nettopreis:float):
        self.__name = name
        self.__artikelnummer = artikelnummer
        self.__anzahl = anzahl
        self.__nettopreis = nettopreis

    def __str__(self):
        return "Rechnungsposition: {0} {1} {2} {3:0.2f}".format(self.__name,
                                                                self.__artikelnummer,
                                                                self.__anzahl,
                                                                self.__nettopreis)
```

[Link zum Python-Code](https://gitlab.com/oer-informatik/uml/umlklasse/-/raw/main/src/rechnung_rechnungsposition_komposition.py)

</span>

#### Für Experten / Hintergrund

Die UML lässt die Möglichkeit, dass Teile vom Ganzen entkoppelt werden und dann auch nicht mit dem Ganzen gelöscht werden. Um diesen Sonderfall abzubilden, müsste man eine _Getter_-Methode implementieren, die innerhalb des Ganzen die Referenz auf das Teil löscht, sobald es das Ganze verlässt. In obigem Beispiel könnte so eine Methode in Rechnung etwa so aussehen:

```java
    public Rechnungsposition removeRechnungsposition(int index) {
         return rechnungspositionen.remove(index);
    }
```

### Übersicht von Assoziation, Aggregation und Komposition

Die unterschiedlichen Objektbeziehungen anhand der oben genannten Beispiele ergeben insgesamt die Modellierung eines Abrechnungssystems:

![Die unterschiedlichen Objektbeziehungen zwischen Konto, Kunde, Rechnung und Rechnungsposition als UML-Klassendiagramm](plantuml/05-Klassendiagramm-Objektbeziehungen.png)


### Das ganze Beispiel als Java-Code

Wer versucht hat, das ganze per UML geplante OOP-Programm in Java nachzuvollziehen, sollte etwa diesen Quelltext am Ende vor sich haben und ausführen können: <button onclick="toggleAnswer('sourcecode')">Gesamtquelltext ein/ausblenden</button>

<span class="hidden-answer" id="sourcecode">

Alle Klassen aus den vergangenen UML-Modellierungsbeispielen spannen folgende Implementierung auf:

Starterklasse `Investmentrechner` in Datei `Investmentrechner.java`:

```java
package de.oerinformatik.crm;

import de.oerinformatik.abrechnung.Buchungen;

public class Investmentrechner {
    public static void main(String[] args) {
        Konto hannesKonto = new Konto("DE123");
        Konto tobiasKonto = new Konto("456", "10010010");
        
        Buchungen meineBuchungen = new Buchungen();
        meineBuchungen.setKonto(tobiasKonto);

        System.out.println(Konto.listeStatusAllerKonten());

        Kunde kundeMustermann = new Kunde(1000.0, "Max Mustermann");
        Rechnung versandRechnung = new Rechnung();
        versandRechnung.addRechnungsposition("Festplatte WDRedLabel10", "12345", 2, 123.45);
        versandRechnung.addRechnungsposition("Cherry Tastatur", "54321", 1, 54.32);
        versandRechnung.setRechnungsbetrag(100.0);
        kundeMustermann.bezahleRechnung(versandRechnung);
        System.out.println(versandRechnung);
    }
}
```

Die `Konto`-Klasse und ihre Spezialisierungen in der Datei `Konto.java`

```java
package de.oerinformatik.crm;

import java.util.ArrayList;

public class Konto{
    private String iban;
    protected double kontostand =0.0;
    static ArrayList<Konto> kontenListe = new ArrayList<>();
    
    public String getIban() {
        return iban;
      }
    
    public void setIban(String iban) {
        this.iban = iban;
    }

    void einzahlen(double betrag) {
      kontostand = kontostand + betrag;
    }

    boolean auszahlen(double betrag) {
        if (kontostand - betrag > 0){
            kontostand = kontostand - betrag;
            return true;
        }else{
            return false;
        }
    }
    
    static String listeStatusAllerKonten(){
        String ausgabe ="";
        for (Konto einKonto: kontenListe){
            ausgabe = ausgabe + "\n" + einKonto.kontostatus();
        }
        return ausgabe;
    }
    
    String kontostatus(){
        return "KontoNr "+this.iban+" Kontostand: "+this.kontostand.toString();
    }

    Konto(String iban){
        this.iban = iban;
        kontenListe.add(this);
    }

    Konto(String ktn, String blz){
        this.iban = "DE__"+blz+ktn;
        kontenListe.add(this);
    }

    public double getKontostand() {
        return kontostand;
    }
  }

  class PrivatKonto extends Konto{
    private String vorname;
    private String nachname;

    public PrivatKonto(String iban, String vorname, String nachname){
        super(iban);
        this.vorname = vorname;
        this.nachname = nachname;
    }
    public String getPrivatkundenName(){
      return this.vorname + " " + this.nachname;
    }   
}

class GeschaeftsKonto extends Konto {

    private String firmenname;

    private double verfuegungsrahmen;

    public double getVerfuegungsrahmen() {
        return verfuegungsrahmen;
    }

    public void setVerfuegungsrahmen(double verfuegungsrahmen) {
        this.verfuegungsrahmen = verfuegungsrahmen;
    }

    public GeschaeftsKonto(String iban, String firmenname, double verfuegungsrahmen) {
        super(iban);
        this.firmenname = firmenname;
        this.setVerfuegungsrahmen(verfuegungsrahmen);
    }

    @Override
    public boolean auszahlen(double betrag) {
      if (betrag > verfuegungsrahmen) {
          return false;
      } else {
          return super.auszahlen(betrag);
      }  
    }  
  }

class SparKonto extends PrivatKonto{
    private double zinssatz;
    public double getZinssatz() {return zinssatz;}
    public void setZinssatz(double zinssatz) {this.zinssatz = zinssatz;}

    public SparKonto(String iban , String vorname, String nachname, double startzins ){
        super(iban, vorname, nachname);
        this.setZinssatz(startzins);
    }

    public void verzinse(){
        this.kontostand = this.kontostand * (1+zinssatz);
    }  
}

```

Die Klasse `Kunde` in der Datei `Kunde.java`:

```java
package de.oerinformatik.crm;

import java.util.ArrayList;

public class Kunde {
    private double vermoegen;
    private String name;

    public void bezahleRechnung(Rechnung rechnung){
        vermoegen -= rechnung.getRechnungsbetrag();
        rechnung.setBezahlt(true, this);
    }

    Kunde(double vermoegen, String name){
        this.vermoegen = vermoegen;
        this.name = name;
    }

    private ArrayList<Konto> kontenListe;
    
    public void addKonto(Konto konto){
        this.kontenListe.add(konto);
    }

    public double gesamtVermoegen(){
        double vermoegen = 0;
        for(Konto konto:kontenListe){
            vermoegen+=konto.getKontostand();
        }
        return vermoegen;
    }

    public String toString(){
        return "Kunde: "+this.name;
    }
}

```

Die Klassen `Rechnung` und `Rechnungsposition` in der Datei `Rechnung.java`:

```java
package de.oerinformatik.crm;

import java.util.ArrayList;

public class Rechnung {
    private double rechnungsbetrag;
    private boolean bezahlt;
    private String bezahltVon = "";

    public double getRechnungsbetrag(){
        return rechnungsbetrag;
    }

    public void setRechnungsbetrag(double rechnungsbetrag){
        this.rechnungsbetrag = rechnungsbetrag;
    }

    public void setBezahlt(boolean bezahlt, Kunde kunde) {
        this.bezahlt = bezahlt;
        bezahltVon = kunde.toString();
        System.out.println("wurde gezahlt von "+bezahltVon);
    } 

   private ArrayList<Rechnungsposition> rechnungspositionen = new ArrayList<Rechnungsposition>();

   public void addRechnungsposition(String name, String artikelnummer, Integer anzahl, double nettopreis) {
       rechnungspositionen.add(new Rechnungsposition(name, artikelnummer, anzahl, nettopreis));
   }

   public Rechnungsposition removeRechnungsposition(int index) {
    return rechnungspositionen.remove(index);
    }

   @Override
   public String toString() {
       String text = "Rechnung: \n";
       for (Rechnungsposition position : rechnungspositionen) {
           text += rechnungspositionen.toString() + "\n";
       }
       if (this.bezahlt){
        text += "Rechnung wurde gezahlt von "+bezahltVon+ "\n";
       }else{
        text += "Rechnung ist noch offen"+ "\n";
       }
       return text;
    } 
}

class Rechnungsposition {

    private String name;
    private String artikelnummer;
    private Integer anzahl;
    private double nettopreis;

    Rechnungsposition(String name, String artikelnummer, Integer anzahl, double nettopreis) {        this.name = name;
        this.artikelnummer = artikelnummer;
        this.anzahl = anzahl;
        this.nettopreis = nettopreis;
    }
 
    @Override
    public String toString() {
        return "Name: " + this.name + " /  Artikelnummer: " + artikelnummer + " / Anzahl: " + this.anzahl + " / Nettoeinzelpreis: " + this.nettopreis;
    }    
}
```

</span>

### Nächster Schritt: Klassenbeziehungen

Wir haben jetzt die Beziehungen behandelt, die konkrete Instanzen (also Objekte) untereinander haben können. Im folgenden Schritt soll es darum gehen, dass wir Verhalten und Strukturen, die in Klassen definiert sind, an andere Klassen vererben können: es geht also um [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung).

### Links und weitere Informationen

- [Primärquelle: Klassendiagramme werden in Kapitel 11.4 der aktuellen Spezifikation von UML definiert: https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)
