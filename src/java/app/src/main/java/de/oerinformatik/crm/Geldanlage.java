package de.oerinformatik.crm;

import java.time.LocalDate;
import java.time.Period;

public abstract class Geldanlage {
    private String name;
    
    public Geldanlage(String name){
        this.setName(name);
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public abstract double getAnlagewert(); 
}

class Aktie extends Geldanlage{
    private int gesamtanzahl = 0;
    private double kurswert = 0;

    public Aktie(String name){
        super(name);
    }

    public double getAnlagewert(){
        return this.gesamtanzahl * this.kurswert;
    }

    public void kaufeAktie(int anzahl){
        gesamtanzahl += anzahl;
    }

    public boolean verkaufeAktie(int anzahl){
        if (anzahl>=gesamtanzahl){
            gesamtanzahl -= anzahl;
            return true;
        }
        return false;
    }

    public void setKurswert(double kurswert){
        this.kurswert = kurswert;
    }
}

class Festgeld extends Geldanlage{
    private double betrag = 0.0;
    private LocalDate startdatum;
    private LocalDate enddatum;
    private double zins;

    public Festgeld(double betrag, LocalDate startdatum, LocalDate enddatum, double zins){
        super("Festgeld");
        this.betrag = betrag;
        this.enddatum = enddatum;
        this.startdatum = startdatum;
        this.zins = zins;
    }

    public double getAnlagewert(){
        Period dauer = Period.between(startdatum, LocalDate.now());
        System.out.println("Dauer des Investments: "+dauer.toString());
        int days = dauer.getDays();
        System.out.println("Dauer in Tagen: "+Integer.toString(days));
        double virtuellesKapital = this.betrag * Math.pow((1 + this.zins),(days/360.0));
        System.out.println("Virtuelles Kapital: "+Double.toString(virtuellesKapital));
        return virtuellesKapital;
    }
}
