class Konto:

    konten = []
    
    def __init__(self):       
        self.iban: str
        self.kontostand: float
        self.kontostand = 0
        Konto.konten.append(self)

    def einzahlen(self, betrag:float):
        self.kontostand += betrag

    @staticmethod
    def liste_status_aller_konten():
        ausgabe = ""
        for konto in Konto.konten:
            ausgabe += konto.konto_status()
        return ausgabe

    def konto_status(self):
        return "Kontostand: {0:0.2f} € \n".format(self.kontostand)
    
mein_konto = Konto()
mein_konto.einzahlen(199.5)
dein_konto = Konto()
dein_konto.einzahlen(123.45)
print(Konto.liste_status_aller_konten())
