## UML-Objektdiagramm

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/113452747566169909</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-objektdiagramm</span>

> **tl/dr;** _(ca. 5 min Lesezeit): In UML Objektdiagramm können Instanzen von Klassen dargestellt werden. Attributwerte werden als Slots oder über Links repräsentiert, Aggregationen und Instanziierungen können ebenso festgehalten werden. Instanzen werden wie Klassen dargestellt, deren Instanzname unterstrichen ist. In diesem Blogpost werden die relativ wenigen Notationsmittel des UML-Objektdiagramms vorgestellt._

Dieser Text ist ein Teil der Infotext-Serie zu UML-Klassendiagrammen:

* [Grundlagen UML-Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm)

* [Sichtbarkeitsmodifikatoren](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)

* [Objektbeziehungen (Assoziation/Aggregation/Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation)

* [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung)

* [abstrakte Klassen und Interfaces](https://oer-informatik.de/uml-klassendiagramm-interface-abstraktion)

* [Polymorphie in der objektorientierten Programmierung](https://oer-informatik.de/uml-klassendiagramm-polymorphie)

* [UML Objektdiagramm zur Beschreibung von konkreten Instanzen](https://oer-informatik.de/uml-objektdiagramm)

* [UML-Klassendiagramme mit PlantUML erstellen](https://oer-informatik.de/uml-klassendiagramm-plantuml)


### Darstellung der Instanziierung

Mit der in den Klassen festgelegten Struktur können Objekte (Instanzen einer Klasse) erstellt werden. Diese Objekte teilen ein Verhalten, festgelegt in den Objektmethoden. Das Verhalten nutzt und verändert den jeweiligen Objektzustand. Der Objektzustand gliedert sich für alle Objekte in der gleichen Struktur, alle Objekte einer Klasse habe die gleichen Attribute. Die Ausprägung des Zustands, also die Attributwerte, sind aber für jedes Objekt individuell. Diese Verknüpfung von geteiltem Verhalten und individuellem Zustand ist der Kern der objektorientierten Programmierung.

Im UML-Objektdiagramm (eine Art Spezialisierung des UML-Klassendiagramms) können auch Objekte und deren individueller Zustand (Attributwerte) dargestellt werden. Wir sehen hier drei Objekte, die unterschiedlich bezeichnet sind: in einem Fall ist nur der Name der Instanz und der Klasse durch Doppelpunkt getrennt genannt (`maxKonto`), in einem zweiten ist nur der Instanzname verzeichnet(`cemKonto`) und im dritten Fall ist für ein anonymes Objekt nur der Klassenname gegeben, zu erkennen am führenden Doppelpunkt (`:Konto`). 

![Drei Objekte instanziieren die Klasse Konto](plantuml/11-Objektdiagramm-instanziierung.png)

Objekte werden in der UML wie Klassen als Rechtecke dargestellt, allerdings werden die Namen- und Klassenbezeichnungen unterstrichen. Die Einfärbung hier dient nur der Unterscheidung, nach UML ist sie nicht nötig. 
Die drei Instanzen werden durch Aufruf der Konstruktor-Methode der Klasse `Konto` erzeugt, sie hängen also ab von der Klasse und deren Konstruktor. Im Objektdiagramm kennzeichnet ein _Dependency_ Pfeil diese Abhängigkeit: er zeigt auf den Konstruktor der Klasse und wird mit dem Stereotyp `<<instantiate>>` markiert. 

### Attributwerte primitiver Datentypen als Literale

Wenn als Attributwert einfache Datentypen (Zahlen, Text) zugewiesen werden sollen, so können diese als Literal direkt per Zuweisungsoperator notiert werden. _Literal_ nennt man Zeichenfolgen, die Werte (also vor allem Zahlen oder Texte) repräsentieren (also: `123`, `3.14`, `"Hallo"`, `0b0001`, `0xFFFFFF`). Der Datentyp muss im Objektdiagramm nicht wiederholt werden, da er bereits im Klassendiagramm angegeben wurde (ist also optional). 

Jede Zeile, in der ein Attributwert zugewiesen wird, nennt man im UML-Objektdiagramm einen _Slot_. 

![Zuweisung der Attributwerte für `iban`, `kontostandInCent` und `zinsatz`](plantuml/11-Objektdiagramm-AttributwerteLiterale.png)

Die Klasse selbst wird in Objektdiagrammen häufig nicht mit angegeben. Sofern die Datentypen oder die Methoden der Klasse aber relevant sind, kann die zugehörige Klasse auch dargestellt werden. 

### Komplexe Attribute / Aggregationen

Attributwerte können nicht nur primitive Datentypen sein, die als Literal angegeben werden. Attributwerte können auch Objekte sein. Es liegt dann eine Teil/Ganzes-Beziehung vor - je nach Ausprägung als [Aggregation oder Komposition (siehe anderer Artikel)](https://oer-informatik.de/uml-klassendiagramm-assoziation).

Es gibt zwei Möglichkeiten, diese im Objektdiagramm darzustellen: man kann dem Wert des Attributs den Namen des Objekts textuell zuweisen (oberes Beispiel): Das Objekt `cem` ist als Kontoinhaber von `cemKonto` eingetragen. Bei vielen Querverknüpfungen ist diese textuelle Variante übersichtlicher.

![Zuweisung von Objekten als Attributwerte (Kontoinhaber - Konto)](plantuml/11-Objektdiagramm-AttributwerteObjekteZuweisung.png)

Die zweite grafische Variante ist bei einfachen Strukturen schneller erschließbar: die Zuweisung wird als _Link_ angegeben. _Links_ sind konkrete Ausprägungen von Assoziationen und werden wie diese notiert. Vom Attributeigentümer gehen die Links in Richtung der Attributinstanzen. Der Attributname wird am Link gegenüber des Attributeigentümers notiert - oft vereinfacht es das Lesen, wenn die Navigationsrichtung mit angegeben wird. Lediglich Multiplizitäten werden hier nicht angegeben, da es sich ja um konkrete, existierende Beziehungen handelt (somit die Multiplizität "1" in jedem Fall gegeben ist).

![Zuweisung von Objekten als Attributwerte (Kontoinhaber - Konto)](plantuml/11-Objektdiagramm-AttributwerteObjekteLink.png)


### Teil/Ganzes-Beziehungen mit Sammlungen (Aggregation, Komposition)

In obigem Beispiel wurde einem Konto ein Kontoinhaber zugewiesen. Wie wird aber eine Teil-Ganzes-Beziehung (Aggregation) im Objektdiagramm modelliert, wenn wir mehrere Teile in einer Sammlung (z.B. in einem Array oder einer Liste) vorhalten?

Das Ausgangsbeispiel, dass ich im Abschnitt des Klassendiagramms genutzt hatte, sieht so aus: viele `Konto` sind in einer `kontenListe` eines Kunden aggregiert:

![Aggregation zwischen Kunde und Konto im UML-Klassendiagramm](plantuml/05-Klassendiagramm-Aggregation.png)

Im Klassendiagramm weist die Aggregation eine Multiplizität von "0 oder viele" auf Seiten der Konten auf. Anstelle der einen Aggregation mit vielfacher Multiplizität treten mehrere _Links_, die jeweils als konkrete Ausprägung einer konkreten Aggregation verstanden werden können. Konkret: Wir weisen mehrere Kontoobjekte einem Kunden zu:

![Aggregation zwischen Kunde und Konto im UML-Objektdiagramm, dargestellt über Links](plantuml/11-Objektdiagramm-AggregationLink.png)

Denkbar, aber nicht so übersichtlich, wäre auch hier die zweite Variante: wir weisen die `Konto`-Objekte per Zuweisung im _Slot_ `kontoListe` dem `Kunde`-Objekt zu:

![Aggregation zwischen Kunde und Konto im UML-Objektdiagramm, dargestellt über Zuweisungen](plantuml/11-Objektdiagramm-AggregationZuweisung.png)

Im UML-Objektdiagramm wird nicht unterschieden, ob das Teil auch ohne das Ganze existiert - sofern diese Unterscheidung erforderlich ist, könnte das Ende des Links entsprechend mit Rauten (ausgefüllt oder nicht) versehen werden. Dies ist im Objektdiagramm aber unüblich.

### Big Picture

Alle Notationsmittel und Verbindungen - gemeinsam mit einer Polymorphen Struktur (`kontoliste`) sind im folgenden Diagramm zusammengefasst:

![UML-Objektdiagramm mit allem PiPaPo](plantuml/11-Objektdiagramm-BigPicture.png)

### Links / weitere Quellen

In der  [UML-Spezifikation zu Objektdiagrammen (Kapitel 9.8.3)](https://www.omg.org/spec/UML/2.5.1/PDF#%5B%7B%22num%22%3A565%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C114.3%2C735.3%2C0%5D) finden sich relativ wenige Informationen und Regelungen - was darauf schließen lässt, dass der praktische Einsatz dieser Diagrammform auf sehr einfache Anwendungsfälle begrenzt ist. Hilfreich ist - wie häuftig - noch die Tabelle am Ende der Spezifikation (Anhang B6 ab S. 705 unten).



