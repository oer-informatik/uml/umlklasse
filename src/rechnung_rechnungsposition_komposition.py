
class Rechnung:
    def __init__(self):
        self.__rechnungspositionen = []

    def add_rechnungsposition(self, name:str, artikelnummer:str, anzahl: int, nettopreis:float):
        rechnungsposition = Rechnungsposition(name, artikelnummer, anzahl, nettopreis)
        self.__rechnungspositionen.append(rechnungsposition)


    def __str__(self):
        text = ""
        for position in self.__rechnungspositionen:
            text += str(position) + "\n"
        return text
    
class Rechnungsposition:
    def __init__(self, name:str, artikelnummer:str, anzahl: int, nettopreis:float):
        self.__name = name
        self.__artikelnummer = artikelnummer
        self.__anzahl = anzahl
        self.__nettopreis = nettopreis

    def __str__(self):
        return "Rechnungsposition: {0} {1} {2} {3:0.2f}".format(self.__name,
                                                                self.__artikelnummer,
                                                                self.__anzahl,
                                                                self.__nettopreis)
    
meine_rechnung = Rechnung()
meine_rechnung.add_rechnungsposition("Bananen", "0815", 2, 3.59)
meine_rechnung.add_rechnungsposition("Äpfel", "0007", 10, 1.99)
meine_rechnung.add_rechnungsposition("Birnen", "0123", 1, 2.19)
print(meine_rechnung)
