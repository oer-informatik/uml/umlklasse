## Klassenbeziehungen: Vererbung von Klassen in der UML

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-klassendiagramm-vererbung</span>

> **tl/dr;** _(ca. 15 min Lesezeit): Zentrales Element der Code-Wiederverwendung in der objektorientierten Programmierung (OOP) ist die Vererbung. Am Beispiel eines Investments werden hier das OOP-Prinzip Vererbung und die darauf aufbauenden Konzepte (z.B. Überschreiben) erklärt._

Dieser Text ist ein Teil der Infotext-Serie zu UML-Klassendiagrammen:

* [Grundlagen UML-Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm)

* [Sichtbarkeitsmodifikatoren](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)

* [Objektbeziehungen (Assoziation/Aggregation/Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation)

* [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung)

* [abstrakte Klassen und Interfaces](https://oer-informatik.de/uml-klassendiagramm-interface-abstraktion)

* [Polymorphie in der objektorientierten Programmierung](https://oer-informatik.de/uml-klassendiagramm-polymorphie)

* [UML-Klassendiagramme mit PlantUML erstellen](https://oer-informatik.de/uml-klassendiagramm-plantuml)

In der _objektorientierten Programmierung_ (OOP) wird das Konzept Vererbung genutzt, um Struktur und Verhalten von Klassen mehrfach nutzen zu können. Man erhält so die Möglichkeit, spezialisierte Klasse auf Grundlage einer allgemeineren Klasse zu bilden. Die spezialisierten Klassen können Struktur und Verhalten der vererbenden Klasse übernehmen oder anpassen - die Implementierung der vererbenden Klasse ist die Basis, die von den erbenden Klassen übernommen werden kann.

![Die Klassen PrivatKonto und Geschäftskonto erben von Konto](plantuml/03_Klassendiagramm-Vererbung.png)

Wir betrachten eine allgemeinere Klasse `Konto` und erbende Klassen `PrivatKonto` und `GeschäftsKonto`. In der UML wird die Vererbung über einen Pfeil mit nicht ausgefüllter dreieckiger Pfeilspitze, die auf die allgemeinere Klasse zeigt, notiert.

Die Klasse `Konto` verfügt bereits über die Basisfunktionalitäten, die ein Konto benötigt: die Attribute `iban` und `kontostand`, die Methoden `einzahlen()` und `auszahlen()`.

Jedoch unterscheiden sich die spezialisierten Konten von der allgemeinen Form darin, dass sie auf unterschiedliche Weise die Kontoinhaber erfassen: der Kontoinhaber eines Privatkontos hat einen Vor- und Nachnamen, während bei einem Geschäftskonto eine Firmenbezeichnung und eine Geschäftsform zur Identifizierung dient.

In der Literatur finden sich eine Vielzahl von Bezeichnungen für diese Beziehung zwischen zwei Klassen:

|Konto| Privatkonto<br/>Geschäftskonto|
|---|---|
|Superklasse|Subklasse|
|Elternklasse|Kindklasse|
|Oberklasse|Unterklasse|
|Basisklasse|abgeleitete Klasse|
|generalisierte Klasse| spezialisierte Klasse|
|allgemeine Klasse|spezifische Klasse|

### Das Ausgangsbeispiel als Quelltext

#### Die Basisklasse `Konto`

Konto verfügt über zwei Attribute, drei Methoden und einen Konstruktor:

![Die Klassen Konto im Detail](plantuml/03_Klassendiagramm-Vererbung-Konto.png)

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

Als Java-Quelltext könnte die Klasse beispielsweise so aussehen:

```java
public class Konto {

    private String iban;
    private double kontostand;

    public Konto(String iban) {
        this.iban = iban;
        kontostand = 0;
    }

    public void einzahlen(double betrag) {
        kontostand = kontostand + betrag;
    }

    public boolean auszahlen(double betrag) {
        if ((kontostand - betrag) > 0) {
            kontostand = kontostand - betrag;
            return true;
        } else {
            return false;
    }   }


    public double getKontostand() {
        return kontostand;
}   }
```
</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Konto:
    def __init__(self, iban: str) -> None:
        self.__iban: str 
        self.__iban = iban
        self.__kontostand: float 
        self.__kontostand = 0
    
    @property
    def kontostand(self) -> float:
        return self.__kontostand

    def einzahlen(self, betrag: float) -> None:
        self.__kontostand += betrag

    def auszahlen(self, betrag: float) -> bool:
        if (kontostand-betrag) >= 0:
            self.__kontostand -= betrag
            return True
        else:
            return False
```

</span>

### Das Konzept der _Erweiterung_ in der abgeleiteten Klasse `PrivatKonto`

`PrivatKonto` verfügt über alle Methoden und Attribute von `Konto`, erweitert diese aber um zusätzliche Attribute und Methoden (`vorname`, `nachname`, `getPrivatekundeName()`). Weiterhin verfügt `PrivatKonto` über einen eigenen Konstruktor, der alle erforderlichen Attribute setzt:

![Die Klassen PrivatKonto im Detail](plantuml/03_Klassendiagramm-Vererbung-PrivatKonto.png)


<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

Java-Quelltext für diese Klasse sähe beispielsweise so aus:

```java
public class PrivatKonto extends Konto{
    private String vorname;
    private String nachname;

    public PrivatKonto(String iban, String vorname, String nachname){
        super(iban);
        this.vorname = vorname;
        this.nachname = nachname;
    }
     public String getPrivatkundenName(){
         return this.vorname + " " + this.nachname;
}    }
```

In der Programmiersprache Java ist es erforderlich, dass der Konstruktor der Superklasse (`Konto(iban)`) in der ersten Zeile der Subklasse aufgerufen wird. Dies geschieht über die Referenz mit dem Keyword `super(...);`


</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Privatkonto(Konto):

    def __init__(self, iban, vorname: str, nachname: str) -> None:
        super().__init__(iban)
        self.__vorname: str 
        self.__vorname = vorname
        self.__nachname: str 
        self.__nachname = nachname
    
    @property
    def privatkundenname(self) ->str:
        return self.__vorname + " " + self.__nachname
```

In der Programmiersprache Python ist es erforderlich, dass der Konstruktor der Superklasse (`Konto.__init__(iban)`) in der ersten Zeile der Subklasse aufgerufen wird. Dies geschieht über die Referenz mit dem Keyword `super().__init__(...);`

</span>

Die vorhandene Basisklasse `Konto` wird durch eine Methode, einen Konstruktor und zwei Attribute erweitert.

Auf die Attribute `iban` und `kontostand` der Basisklasse kann die Klasse `PrivatKonto` nicht zugreifen, da diese die Sichtbarkeit `private` haben. Der Zugriff kann nur über die öffentlichen Methoden von `Konto` erfolgen (in Python: über die Properties).

### Das Konzept _Überschreiben_ - die abgeleitete Klasse `GeschäftsKonto`

![Die Klassen GeschäftsKonto im Detail](plantuml/03_Klassendiagramm-Vererbung-GeschäftsKonto.png)

In der Klasse `GeschaeftsKonto` wird die Basisklasse `Konto` wieder um einige Attribute (`firmenname`, `verfuegungsrahmen`) und Methoden (Konstruktor, Getter und Setter) erweitert.

Zusätzlich wird jedoch auch eine Methode _überschrieben_: Die Methode `auszahlen()` wird in Geschäftskonto neu implementiert. Es soll hier zusätzlich der Verfügungsrahmen geprüft werden.

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

Eine beispielhafte Implementierung in Java könnte etwa so aussehen:

```java
public class GeschaeftsKonto extends Konto {

    private String firmenname;

    private double verfuegungsrahmen;
    public double getVerfuegungsrahmen() {return verfuegungsrahmen;}
    public void setVerfuegungsrahmen(double verfuegungsrahmen) {this.verfuegungsrahmen = verfuegungsrahmen;}

    public GeschaeftsKonto(String iban, String firmenname, double verfuegungsrahmen) {
        super(iban);
        this.firmenname = firmenname;
        this.setVerfuegungsrahmen(verfuegungsrahmen);
    }

    @Override
    public boolean auszahlen(double betrag) {
      if (betrag > verfuegungsrahmen) {
          return false;
      } else {
          return super.auszahlen(betrag);
}  }  }
```

Die Annotation `@Override` kann in Java vor Methoden geschrieben werden, die eine bereits vorhandene Methode einer Superklasse überschreiben. `Konto.auszahlen()` bleibt vorhanden, wird aber von der überschreibenden Methode verdeckt.

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Geschaeftskonto(Konto):
    def __init__(self, iban, firmenname: str, verfuegungsrahmen: float) -> None:
        super().__init__(iban)
        self.__firmenname: str 
        self.__firmenname = firmenname
        self.__verfuegungsrahmen: float
        self.__verfuegungsrahmen = verfuegungsrahmen
    
    @property
    def verfuegungsrahmen(self) ->float:
        return self.__verfuegungsrahmen
    
    @verfuegungsrahmen.setter
    def verfuegungsrahmen(self, verfuegungsrahmen):
        self.__verfuegungsrahmen = verfuegungsrahmen

    def auszahlen(self, betrag: float) -> bool:
        if (betrag) > self.__verfuegungsrahmen:
            return False
        else:
            return super().auszahlen(betrag)

```

</span>

Da Geschäftskonto aufgrund der Sichtbarkeitsmodifikatoren in `Konto` den Kontostand nicht selbst verändern kann nutzt es die Methoden von Konto, um die Auszahlung durchzuführen. Über die `super`-Referenz wird wieder auf die Superklasse verwiesen und mit `super.auszahlung(betrag)` eine Methode aufgerufen, die den Kontostand ändern darf (da sie Bestandteil von Konto ist und somit auf `private` Attribute von Konto zugreifen darf).

### Das Konzept der partiellen Sichtbarkeit (`protected`)

Eine erbende Klasse von `Konto` kann das Attribut `kontostand` nicht verändern. Wenn wir für eine neue Klasse `SparKonto` eine Methode `verzinse()` schreiben wollen, so kann diese derzeit nicht direkt den Kontostand um die aufgelaufenen Zinsen erhöhen.

Es gibt viele vergleichbare Anwendungsfälle, in denen wichtig ist, dass auch die erbenden Klassen Zugriff auf geschützte Attribute und Methoden haben. Daher stellt die UML (und viele OOP-Sprachen, jedoch nicht Python) einen weiteren Sichtbarkeitsmodifikator zur Verfügung, der Zugriff innerhalb der Klasse und für alle erbenden Klassen einräumt: `protected` (im UML-Klassendiagramm: `#`).

![Das Attribut Kontostand wird auf `protected` gesetzt](plantuml/03_Klassendiagramm-Vererbung-protected.png)

Im obigen UML-Klassendiagramm ist `kontostand` als `protected` notiert (mit `#`). Somit hat die über `PrivatKonto` erbende Klasse `Sparkonto` Zugriff auf das Attribut `kontostand`. 


<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

Im Java Quelltext dargestellt wären folgende Anpassungen nötig: In der Klasse `Konto` muss der Sichtbarkeitsmodifikator von `private` auf `protected` geändert werden.

```java
    protected double kontostand;
```

Somit hätte eine Klasse `Sparkonto` die Möglichkeit, die Zinsen in der Methode `verzinse()` direkt aufzuschlagen:

```java
public class SparKonto extends PrivatKonto{
    private double zinssatz;
    public double getZinssatz() {return zinssatz;}
    public void setZinssatz(double zinssatz) {this.zinssatz = zinssatz;}

    public SparKonto(String iban , String vorname, String nachname, double startzins ){
        super(iban, vorname, nachname);
        this.setZinssatz(startzins);
    }

    public void verzinse(){
        this.kontostand = this.kontostand * (1+zinssatz);
}  }
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

In Python gibt es keinen Zugriffsmodifikator für `protected`. Die Philosophie der Sprache setzt voraus, dass Entwickler verantwortlich mit dem Code umgehen, und sich daher nicht selbst aus dem Code aussperren müssen. 

_Workaround 1: "Markieren" per Unterstrich:_

Als Workaround kann das Markieren der Attribute mit einem einzelnen Unterstrich in der Klasse `Konto` genutzt werden (`_kontostand`). Das Attribut bleibt öffentlich lesbar, aber die Konvention sagt, man soll es nicht direkt verwenden.

```python
class Konto:
    def __init__(self, iban: str) -> None:
        self._iban: str 
        self._iban = iban  # von außen über kontoobject._iban zugreifbar, defacto public
        self.__kontostand: float 
        self.__kontostand = 0
```

Diese Schreibweise ist Geschmackssache und die Frage bleibt, ob Aufwand und Nutzen in einem Verhältnis stehen. Ich favorisiere die zweite Möglichkeit: das _protected_-Attribut über eine Property öffentlich zugänglich zu machen. 

_Workaround 2: public statt protected_

Das ist natürlich keine Umsetzung von `protected`, aber wäre mein Weg der Wahl, wenn der Entwurf sagt, ein Attribut soll `protected` sein, und ich muss das mit Python implementieren:

```python
class Konto:

    @property
    def kontostand(self) -> float:
        return self.__kontostand

    @kontostand.setter
    def kontostand(self, betrag):
        self.__kontostand = betrag
```

Die Klasse Sparkonto sieht dann so aus:

```python
class Sparkonto(Privatkonto):
    def __init__(self, iban, vorname: str, nachname: str, startzins: float) -> None:
        super().__init__(iban, vorname, nachname)
        self.__zinssatz: float 
        self.__zinssatz = startzins
    
    @property
    def zinssatz(self) ->float:
        return self.__zinssatz

    @zinssatz.setter
    def zinssatz(self, zinssatz):
        self.__zinssatz = zinssatz

    def verzinse(self):
        # Variante 2: Property
        self.kontostand = self.kontostand * (1+self.zinssatz)
        # bei Variante 1 würde es lauten:
        # self._kontostand = self._kontostand * (1+self.zinssatz)
```

</span>

### Das ganze Beispiel als Java-Code

Wer versucht hat, das ganze per UML geplante OOP-Programm in Java nachzuvollziehen, sollte etwa diesen Quelltext am Ende vor sich haben und ausführen können: <button onclick="toggleAnswer('sourcecode')">Gesamtquelltext ein/ausblenden</button>

<span class="hidden-answer" id="sourcecode">

Eine beispielhafte Implementierung in Java könnte etwa so aussehen:

```java
package de.oerinformatik.crm;

import java.util.ArrayList;

public class Konto{
    private String iban;
    protected double kontostand;

    static ArrayList<Konto> kontenListe = new ArrayList<>();
    
    public String getIban() {
        return iban;
      }
    
    public void setIban(String iban) {
        this.iban = iban;
    }

    void einzahlen(double betrag) {
      kontostand = kontostand + betrag;
    }

    boolean auszahlen(double betrag) {
        if (kontostand - betrag > 0){
            kontostand = kontostand - betrag;
            return true;
        }else{
            return false;
        }
    }
    
    static String listeStatusAllerKonten(){
        String ausgabe ="";
        for (Konto einKonto: kontenListe){
            ausgabe = ausgabe + "\n" + einKonto.kontostatus();
        }
        return ausgabe;
    }
    
    String kontostatus(){
        return "KontoNr "+this.iban+" Kontostand: "+this.kontostand.toString();
    }

    Konto(String iban){
        this.iban = iban;
        kontostand =0.0;
        kontenListe.add(this);
    }

    Konto(String ktn, String blz){
        this.iban = "DE__"+blz+ktn;
        kontenListe.add(this);
    }

    public double getKontostand() {
        return kontostand;
    }
  }

  class PrivatKonto extends Konto{
    private String vorname;
    private String nachname;

    public PrivatKonto(String iban, String vorname, String nachname){
        super(iban);
        this.vorname = vorname;
        this.nachname = nachname;
    }
    public String getPrivatkundenName(){
      return this.vorname + " " + this.nachname;
    }   
}

class GeschaeftsKonto extends Konto {

    private String firmenname;

    private double verfuegungsrahmen;

    public double getVerfuegungsrahmen() {
        return verfuegungsrahmen;
    }

    public void setVerfuegungsrahmen(double verfuegungsrahmen) {
        this.verfuegungsrahmen = verfuegungsrahmen;
    }

    public GeschaeftsKonto(String iban, String firmenname, double verfuegungsrahmen) {
        super(iban);
        this.firmenname = firmenname;
        this.setVerfuegungsrahmen(verfuegungsrahmen);
    }

    @Override
    public boolean auszahlen(double betrag) {
      if (betrag > verfuegungsrahmen) {
          return false;
      } else {
          return super.auszahlen(betrag);
      }  
    }  
  }

class SparKonto extends PrivatKonto{
    private double zinssatz;
    public double getZinssatz() {return zinssatz;}
    public void setZinssatz(double zinssatz) {this.zinssatz = zinssatz;}

    public SparKonto(String iban , String vorname, String nachname, double startzins ){
        super(iban, vorname, nachname);
        this.setZinssatz(startzins);
    }

    public void verzinse(){
        this.kontostand = this.kontostand * (1+zinssatz);
    }  
}
```

</span>


