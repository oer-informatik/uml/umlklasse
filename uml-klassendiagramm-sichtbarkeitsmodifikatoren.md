## UML-Klassendiagramm: Sichtbarkeitsmodifikatoren

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren</span>


> **tl/dr;** _(ca. 4 min Lesezeit): Kapselung ist ein Konzept der objektorientierten Programmierung (OOP), das verhindern soll, dass Anpassungen des Codes zu Inkonsistenzen führen. Der Zustand der Objekte soll nicht direkt änderbar sein, sondern nur über Methoden, die ggf. weitere Anpassungen vornehmen können: Getter und Setter._

Dieser Text ist ein Teil der Infotext-Serie zu UML-Klassendiagrammen:

* [Grundlagen UML-Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm)

* [Sichtbarkeitsmodifikatoren](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)

* [Objektbeziehungen (Assoziation/Aggregation/Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation)

* [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung)

* [abstrakte Klassen und Interfaces](https://oer-informatik.de/uml-klassendiagramm-interface-abstraktion)

* [Polymorphie in der objektorientierten Programmierung](https://oer-informatik.de/uml-klassendiagramm-polymorphie)

* [Objektinstanzen im UML-Objektdiagramm darstellen](https://oer-informatik.de/uml-objektdiagramm)

* [UML-Klassendiagramme mit PlantUML erstellen](https://oer-informatik.de/uml-klassendiagramm-plantuml)



### Information Hiding Principle (IHP)

Klassen sollten in der OOP so wenig wie möglich nach außen veröffentlichen. Attribute und Methoden, die lediglich den inneren Zustand eines Objekts betreffen oder nur innerhalb der Objekte benötigt werden, sollten von außen nicht erreichbar sein. Dadurch wird zum einen sichergestellt, dass bei Code-Anpassungen der inneren Struktur der Klasse keine Aufrufe von außen korrumpiert werden. Zum anderen erhält man so definierte Schnittstellen, über die Objekte nach außen kommunizieren. Plausibilisierungen, Zugriffskontrolle und Protokollierung beispielsweise können so viel einfacher umgesetzt werden.

Daher gibt es die Möglichkeit, Attribute und Methoden (beides sind _Member_ einer Klasse) in ihrer _Sichtbarkeit_ und im _Zugriff_ einzuschränken. In einigen Programmiersprachen kann man lediglich Member verstecken, sie sind also nicht ohne weiteres zu finden. Der Zugriff ist aber weiterhin möglich. In anderen Programmiersprachen lässt sich der Zugriff komplett einschränken. Daher verwende ich hier "Sichtbarkeitsmodifikator" und "Zugriffsmodifikator" synonym und beschreibe mit beiden Begriffen das gleiche Konzept (in unterschiedlichen Programmiersprachen).

### Zugriffsmodifikatoren

In der UML unterscheidet man vor allem Member, die nur innerhalb der Instanz einer Klasse zugreifbar und sichtbar sind (`private`, gekennzeichnet mit "-") und Member, die auch von außen aufgerufen werden können (`public`, gekennzeichnet mit "+"). Bei Membern kann es sich sowohl um Attribute als auch um Methoden handeln. Weitere Varianten, die deutlich seltener benutzt werden, sind die Sichtbarkeit innerhalb einer Vererbungsstruktur (`protected`, "#") und innerhalb von Namensräumen / Packages (`package`, "~").

![UML Klassendiagramm mit gekennzeichneten Sichtbarkeitsmodifikatoren vor Membern: - private, + public, # protected und ~ package](plantuml/01_Klassendiagramm-alleSichtbarkeitsmodifizierer-mitKommentar.png)

Eine verbreitete Art, die Sichtbarkeit von Membern über die Zugriffsmodifikatoren `private` und `public` zu konfigurieren, ist die Kapselung von Attributen.

### Kapselung

Sinn und Zweck der Kapselung ist es, den inneren Zustand von Objekten zu verstecken. Hierbei werden Attribute versteckt (`private`) und nur durch öffentliche Zugriffsmethoden (Getter- und Settermethoden, `public`) von Außen verfügbar gemacht. Versteckt werden die Attribute, in dem sie über den Zugriffsmodifikator `private` nur innerhalb des Objekts zugreifbar sind. Je nachdem, ob die Attribute von Außen les- oder änderbar sein sollen werden öffentliche (`public`) Getter- und/oder Settermethoden erstellt:

![Kapselung des Attributs iban mit Getter- und Setter-Methoden.](plantuml/01_Klassendiagramm-Kapselung.png)

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

In Java wäre die Implementierung folgendermaßen:

```java
class Konto{
  private String iban;

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
}  }
```

Der direkte Zugriff auf das Attribut von Außen wäre dann nicht mehr möglich (Auszug aus der Java-Shell):

```java
Konto konto1 = new Konto();
|  konto1 ==> Konto@4671e53b
konto1.iban="DE123";
|  Error:
|  iban has private access in Konto
|  konto1.iban="DE123";
|  ^---------^
```

Über die Getter- und Settermethoden kann jedoch zentral auf das Attribut zugegriffen werden.

```java
konto1.setIban("DE789");
konto1.getIban();
|  $4 ==> "DE789"
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

In `Python` wäre die Implementierung folgendermaßen:

```python
class Konto:

    def __init__(self) -> None:
        self._iban = ""

    def get_iban(self) -> str:
        return self._iban

    def set_iban(self, iban: str):
        self._iban = iban
```

Klassische Sichtbarkeitsmodifikatoren, wie in anderen Programmiersprachen, kennt Python nicht. Daher werden hier Attribute mit doppeltem _Underscore_ geschrieben und sind auf diese Art von Außen nicht direkt ansprechbar. 

Details zum _Information Hiding_ in Java findet sich in [diesem Artikel zu Kapselung in Python](https://oer-informatik.de/python_oop_kapselung).

Ein weiterer verbreiteter Weg, den Zustand von Python-Objekten zu verstecken, sind _Properties_. Auch hierzu findet sich im oben verlinkten Artikel eine Erklärung.

</span>

Durch Kapselung wäre es möglich, dafür zu sorgen, dass nur gültige IBAN-Werte übergeben werden können. Ein zentraler Setter könnte absichern, dass die Gültigkeit einer IBAN immer geprüft ist.

An anderes Beispiel wäre eine Überprüfung des Kontostands, bevor Abhebungen den aktuellen Kontostand verändern. Ein Setter könnte prüfen, ob der resultierende Betrag kleiner als ein Dispolimit ist, und andernfalls die Anpassung verhindern (beispielsweise eine Exception werfen).

Soll beispielsweise eine Telefonnummer immer in internationaler Form ausgegeben werden, so kann es reichen, einen _Getter_ anzupassen. Die Repräsentationsart, in welcher Form der Zustand eines Objekts nach außen dringt, kann somit auch nachträglich angepasst werden, ohne, dass der Zustand selbst veränder werden muss. 

### Zugriffsmodifikation innerhalb von Namensräumen (Packages) 

Die UML stellt zur Gliederung von Klassen Namensräume (_Packages_) zur Verfügung, in den meisten Programmiersprachen sind die unterschiedlichen Module in _Packages_ organisiert. Um eine Möglichkeit zu schaffen, dass Member nicht komplett öffentlich zugreifbar sind, sondern nur innerhalb eines Namensraums, gibt es einen weiteren Zugriffsmodifikator `package`:

![Package Zugriffsmodifikatoren in unterschiedlichen Namensräumen](plantuml/01_Klassendiagramm-SichtbarkeitPackage.png)

Im Beispiel oben ist der Konstruktor `Konto()` nur innerhalb des Paketes `CRM` nutzbar. Die Klasse `Buchungen` nutzt zwar ein `Konto` (und verfügt auch über ein Attribut vom Typ `Konto`), kann dieses jedoch selbst nicht erzeugen, da der Konstruktor _paketsichtbar_ ist (`~Konto()`), und nicht für den Namensraum (also das _package_) `Abrechnung`, in dem `Buchungen` ist. `Buchungen` ist also darauf angewiesen, dass innerhalb des Pakets `CRM` ein `Konto` erzeugt wird und per Setter gesetzt wird. Danach kann `Buchungen` aber auf die `public`-Methoden von `Konto` zugreifen.

In Java ist Paketsichtbarkeit die _default_-Einstellung, daher werden dafür gar keine Sichtbarkeitsmodifizierer angegeben. Im Beispiel oben steht anstelle von `public`, `private` oder `protected` also nichts vor dem Konstruktor `Konto()`:

```java
class Konto{
  Konto(String iban){
    this.setIban(iban);
  }
  ...
}
```

In Python gibt es kein Konstrukt für Paketsichtbarkeit.

### Zugriffsmodifikation innerhalb Vererbungshierarchien 

Es besteht darüber hinaus noch eine vierte Möglichkeit, die Sichtbarkeit von _Membern_ einzuschränken, hierzu müssen wir kurz etwas vorgreifen: Mit `private` annotierte Attribute und Methoden sind auch für Instanzen erbender Klassen (siehe übernächsten Artikel) nicht zugreifbar. Im Beispiel unten verfügt eine Instanz von `PrivatKonto` zwar intern über die Information `iban`, kann aber selbst nicht darauf zugreifen, das es als `private` gekennzeichnet ist. Die erbenden Klassen `PrivatKonto` und `GeschaeftsKonto` müssen also die _Getter_ und _Setter_ nutzen, um `iban` anzusprechen.

Als Zwischenstufe zwischen `private` und `public` kommt hier `protected` (`#`) ins Spiel: So annotierte Member sind	nur innerhalb von Instanzen der eigenen Klasse oder innerhalb abgeleiteter Klassen der Vererbungsstruktur (Subklassen und Subsubklassen) sichtbar/ zugreifbar. Auf das Attribut `kontostand` können `PrivatKonto` und `GeschaeftsKonto` also auch ohne _Getter_ und _Setter_ direkt zugreifen, externe Instanzen aber nicht.

![Protected Zugriffsmodifikatoren in Vererbungsstrukturen](plantuml/01_Klassendiagramm-SichtbarkeitProtected.png)

Im Java-Quellcode wird analog zur UML auch das _Keyword_ `protected` genutzt. Für das Beispiel oben sieht der Codeauszug etwa so aus:

```java
class Konto{
  protected double kontostand;
  ...
}
```

Python verfügt über kein Konzept, das Sichtbarkeiten auf Vererbungsstrukturen begrenzt.

### Übersicht der Zugriffsmodifikatoren

Im Ganzen gibt es vier Sichtbarkeitsmodifikatoren wird im Rahmen der Vererbung / der Packages genauer eingegangen. Die Umsetzung der Berechtigungen in Java ist in folgender Tabelle wiedergegeben, für Python gibt es keine Entsprechungen:

| Umsetzung in Java|Darstellung<br/> im<br/> UML-<br/>Klassen-<br/>diagramm|sieht<br/> eigene<br/> Klasse|sieht<br/> Klasse<br/> im<br/> gleichen<br/> Paket|sieht<br/> Unterklasse<br/> im<br/> anderen<br/> Paket|sieht<br/> Klasse<br/> in anderem<br/> Paket|
|:---|:---|---|---|---|---|
|privat:<br/>`private String iban`|`-iban`|ja|nein|nein|nein|
|öffentlich: <br/> `public double getKontostand()`|`+getKontostand(): double`|ja|ja|ja|ja|
|geschützt<br/>`protected double kontostand`|`#kontostand`|ja|ja|ja|nein|
|paketsichtbar:<br/>`Konto()`|`~Konto()`|ja|ja|nein|nein|


### Das ganze Beispiel als Java-Code

Wer versucht hat, das ganze per UML geplante OOP-Programm in Java nachzuvollziehen, sollte etwa diesen Quelltext am Ende vor sich haben und ausführen können: <button onclick="toggleAnswer('sourcecode')">Gesamtquelltext ein/ausblenden</button>

<span class="hidden-answer" id="sourcecode">

Eine Implementierung der kompletten Beispiele oben im Java erfordert drei Dateien - da in Java in jeder Datei immer nur eine öffentliche (`public`) Klasse sein darf:

Die Datei `Investmentrechner.java` erzeugt Objekte der beiden anderen Klassen und veknüpft diese:

```java
package de.oerinformatik.crm;

import de.oerinformatik.abrechnung.Buchungen;

public class Investmentrechner {
    public static void main(String[] args) {
        Konto hannesKonto = new Konto("DE123");
        Konto tobiasKonto = new Konto("456", "10010010");
        
        Buchungen meineBuchungen = new Buchungen();
        meineBuchungen.setKonto(tobiasKonto);

        System.out.println(Konto.listeStatusAllerKonten());
    }
}
```
Die Datei `Konto.java` enthält neben `Konto` auch die beiden Spezialisierungen aus obigem Beispiel:

```java
package de.oerinformatik.crm;

import java.util.ArrayList;

public class Konto{
    private String iban;
    protected double kontostand =0.0;
    static ArrayList<Konto> kontenListe = new ArrayList<>();
    
    public String getIban() {
        return iban;
      }
    
    public void setIban(String iban) {
        this.iban = iban;
    }

    public boolean einzahlen(double betrag) {
      kontostand = kontostand + betrag;
      return true;
    }

    public boolean auszahlen(double betrag) {
        if (kontostand - betrag > 0){
            kontostand = kontostand - betrag;
            return true;
        }else{
            return false;
        }
    }
    
    static String listeStatusAllerKonten(){
        String ausgabe ="";
        for (Konto einKonto: kontenListe){
            ausgabe = ausgabe + "\n" + einKonto.toString();
        }
        return ausgabe;
    }
    
    String toString(){
        return "KontoNr "+this.iban+" Kontostand: " + Double.toString(this.kontostand);
    }

    Konto(String iban){
        this.iban = iban;
        kontenListe.add(this);
    }

    Konto(String ktn, String blz){
        this.iban = "DE__"+blz+ktn;
        kontenListe.add(this);
    }
  }

  class PrivatKonto extends Konto{
    private String vorname;
    private String nachname;

    public PrivatKonto(String iban, String vorname, String nachname){
        super(iban);
        this.vorname = vorname;
        this.nachname = nachname;
    }
    public String getName(){
      return this.vorname + " " + this.nachname;
    }   
}

class GeschaeftsKonto extends Konto {

    private String firmenname;

    private double verfuegungsrahmen;

    public double getVerfuegungsrahmen() {
        return verfuegungsrahmen;
    }

    public void setVerfuegungsrahmen(double verfuegungsrahmen) {
        this.verfuegungsrahmen = verfuegungsrahmen;
    }

    public GeschaeftsKonto(String iban, String firmenname, double verfuegungsrahmen) {
        super(iban);
        this.firmenname = firmenname;
        this.setVerfuegungsrahmen(verfuegungsrahmen);
    }

    @Override
    public boolean auszahlen(double betrag) {
      if (betrag > verfuegungsrahmen) {
          return false;
      } else {
          return super.auszahlen(betrag);
      }  
    }  
  }

class SparKonto extends PrivatKonto{
    private double zinssatz;
    public double getZinssatz() {return zinssatz;}
    public void setZinssatz(double zinssatz) {this.zinssatz = zinssatz;}

    public SparKonto(String iban , String vorname, String nachname, double startzins ){
        super(iban, vorname, nachname);
        this.setZinssatz(startzins);
    }

    public void verzinse(){
        this.kontostand = this.kontostand * (1+zinssatz);
    }  
}
```

Die Datei `Buchungen.java` liegt in einem anderen Paket wie die beiden anderen Klassen (in Java wird sie in einen anderen Ordner kopiert):

```java
package de.oerinformatik.abrechnung;

import de.oerinformatik.crm.Konto;

public class Buchungen {
    private Konto konto;

    public void setKonto(Konto konto) {
        this.konto = konto;
    }
}
```
</span>

### Nächste Bestandteile des UML-Klassendiagramms:

In weiteren Artikel geht es um andere Eigenschaften des UML-Klassendiagramms, als Nächstes wird auf [Objektbeziehungen (Assoziation, Aggregation, Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation) eingegangen.

### Links und weitere Informationen

- [Primärquelle: Klassendiagramme werden in Kapitel 11.4 der aktuellen Spezifikation von UML definiert: https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)
