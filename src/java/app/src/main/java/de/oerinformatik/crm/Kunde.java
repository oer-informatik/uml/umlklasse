package de.oerinformatik.crm;

import java.util.ArrayList;

public class Kunde {
    private String vorname;
    private String nachname;

    public boolean bezahleRechnung(Rechnung rechnung){
        for(Geldanlage anlage :geldanlagenListe){
            if (anlage instanceof Zahlbar){
                double betrag = rechnung.getRechnungsbetrag();
                if (((Zahlbar)anlage).auszahlen(betrag)){
                    rechnung.setBezahlt(true, this);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean bezahleRechnung(Rechnung rechnung, Zahlbar konto){
        double kosten = rechnung.getRechnungsbetrag();
        return konto.auszahlen(kosten);
    }

    Kunde(String vorname, String nachname){
        this.vorname = vorname;
        this.nachname = nachname;
    }

    private ArrayList<Geldanlage> geldanlagenListe;
    
    public void addGeldanlage(Geldanlage geldanlage){
        this.geldanlagenListe.add(geldanlage);
    }

    public double gesamtVermoegen(){
        double vermoegen = 0;
        for(Geldanlage anlage :geldanlagenListe){
            vermoegen+=anlage.getAnlagewert();
        }
        return vermoegen;
    }

    public String toString(){
        return "Kunde: "+this.vorname + this.nachname;
    }
}
