## UML-Klassendiagramme mit PlantUML zeichnen

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110480281247180661</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-klassendiagramm-plantuml</span>


> **tl/dr;** _(ca. 13 min Lesezeit): Das Tool [PlantUML](https://plantuml.com) ist hervorragend geeignet, um Klassendiagramme direkt in Dokumentationen wie [Readme.md-Dateien des Repositorys](http://oer-informatik.de/git04-readme-erstellen) oder in Kommentaren zu integrieren, sie aus Programmcode direkt zu generieren und sie in einem Versionskontrollsystem zu versionieren. In diesem Infotext stelle ich vor, wie mit PlantUML die unterschiedlichen Notationsmittel von Klassendiagrammen genutzt werden können, mit welchen Einstellungen sie nahe am UML-Standard bleiben und aufgehübscht sind._

Dieser Text ist ein Teil der Infotext-Serie zu UML-Klassendiagrammen:

* [Grundlagen UML-Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm)

* [Sichtbarkeitsmodifikatoren](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)

* [Objektbeziehungen (Assoziation/Aggregation/Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation)

* [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung)

* [abstrakte Klassen und Interfaces](https://oer-informatik.de/uml-klassendiagramm-interface-abstraktion)

* [Polymorphie in der objektorientierten Programmierung](https://oer-informatik.de/uml-klassendiagramm-polymorphie)

* [Objektinstanzen im UML-Objektdiagramm darstellen](https://oer-informatik.de/uml-objektdiagramm)

* [UML-Klassendiagramme mit PlantUML erstellen](https://oer-informatik.de/uml-klassendiagramm-plantuml)


### Das Tool PlantUML

PlantUML ist ein Tool, um alle möglichen (UML-)Diagramme aus deklarativen Beschreibungen (PlantUML-Quelltext) zu generieren. Ein mit PlantUML erstelltes Klassendiagramm sieht beispielsweise so aus:

![Beispiel-Klassendiagramm mit vielen Notationselementen, die im folgenden erklärt werden](plantuml/10-Klassendiagramm-big-picture.png)

Da es aus Quelltext Diagramme erzeugt wird PlantUML häufig mit dem  _Buzzword_  _Diagram as Code_ bezeichnet. Die Diagramme können per Webservice erstellt (z.B. [planttext.com](https://www.planttext.com/) oder [plantuml.com](https://plantuml.com/)), als Code direkt in Markdown-Quelltexten eingebettet oder mithilfe einer [lokalen Installation](https://plantuml.com/de/download) generiert werden.

Eine Dokumentation des Tools findet sich auf der [Internetseite des Projekts](https://plantuml.com/de/class-diagram). 

PlantUML erstellt sehr schnell und einfach Diagramme, ist aber relativ unflexibel, was die Positionierung von Elementen angeht und weicht in der Darstellung oft vom UML-Standard ab.

Über ein paar Umwege kann man die Erstellung jedoch in Aussehen und Struktur beeinflussen. Neben der grundlegenden Syntax von PlantUML stelle ich diese Umwege hier vor.

### Ein erstes UML-Klassendiagramm erstellen

![Default-Symbole von PlantUML](plantuml/01_Klassendiagramm-ohneFormat.png)


PlantUML-Quellcode ist eingefasst zwischen `@startuml` und `@enduml` - die Notation selbst ist angelehnt an gängige Java-Notationen (`class ... {}`) und UML-Eigenschaften (`-, +, #, {abstract}`):

```plantuml
@startuml

class Konto{
	-iban: String
	-kontostand: double
	#{static} kontenListe: Konto[0..*]
	+Konto(iban: String)
	+einzahlen(betrag:double):void
	+auszahlen(betrag:double):boolean
	+getKontostand():double
	+kontoStatus():String
	#{static} listeStatusAllerKonten():String
}
@enduml 
```

Editieren und darstellen kann man die Diagramme am schnellsten über einen Webservice, das obige Beispiel [findet sich unter dieser URL](https://www.planttext.com/?text=RP0z3u8m48Pd-YkQk814ujn9PzmOZKDBBzXuNfEsE4ZuxnuGnC7n_SYRv-uKamwfVw8GBUeOPUqf-K5a1sSqATcauAXZ-PXyBfDLqlhU88XiDx2HN3lAAGMwk9X0BHFNOrNjRo8hPvN_hXNiWgEtlYDGRY05tQbbir0lxopdke__S-CzWYQkT93gBrHUg9Mgd54R9kiZs-i56onEc4j-HeHGp_1RTHGd8Ci_aHy0). Dabei wird der Quelltext als Hashwert übergeben - so lassen sich bearbeitbare Diagramme anlegen und verlinken. Dieser Hashwert kann auch genutzt werden, um [Links zum PNG-Bild](https://www.planttext.com/api/plantuml/png/RP0z3u8m48Pd-YkQk814ujn9PzmOZKDBBzXuNfEsE4ZuxnuGnC7n_SYRv-uKamwfVw8GBUeOPUqf-K5a1sSqATcauAXZ-PXyBfDLqlhU88XiDx2HN3lAAGMwk9X0BHFNOrNjRo8hPvN_hXNiWgEtlYDGRY05tQbbir0lxopdke__S-CzWYQkT93gBrHUg9Mgd54R9kiZs-i56onEc4j-HeHGp_1RTHGd8Ci_aHy0) oder zu einem [SVG-Bild](https://www.planttext.com/api/plantuml/svg/RP0z3u8m48Pd-YkQk814ujn9PzmOZKDBBzXuNfEsE4ZuxnuGnC7n_SYRv-uKamwfVw8GBUeOPUqf-K5a1sSqATcauAXZ-PXyBfDLqlhU88XiDx2HN3lAAGMwk9X0BHFNOrNjRo8hPvN_hXNiWgEtlYDGRY05tQbbir0lxopdke__S-CzWYQkT93gBrHUg9Mgd54R9kiZs-i56onEc4j-HeHGp_1RTHGd8Ci_aHy0) zu erhalten. Als Besonderheit generiert PlanUML auch [ASCII-Art-Diagramme](https://www.planttext.com/api/plantuml/txt/RP0z3u8m48Pd-YkQk814ujn9PzmOZKDBBzXuNfEsE4ZuxnuGnC7n_SYRv-uKamwfVw8GBUeOPUqf-K5a1sSqATcauAXZ-PXyBfDLqlhU88XiDx2HN3lAAGMwk9X0BHFNOrNjRo8hPvN_hXNiWgEtlYDGRY05tQbbir0lxopdke__S-CzWYQkT93gBrHUg9Mgd54R9kiZs-i56onEc4j-HeHGp_1RTHGd8Ci_aHy0). Für das obige Beispiel sieht das dann so aus:

```
,-----------------------------------------.
|Konto                                    |
|-----------------------------------------|
|-iban: String                            |
|-kontostand: double                      |
|#{static} kontenListe: Konto[0..*]       |
|+Konto(iban: String)                     |
|+einzahlen(betrag:double):void           |
|+auszahlen(betrag:double):boolean        |
|+getKontostand():double                  |
|+kontoStatus():String                    |
|#{static} listeStatusAllerKonten():String|
`-----------------------------------------'
```

Für jedes Diagramm in diesem Artikel lässt sich der Quelltext schnell nachschlagen: ich habe unter identischem Pfad wie die Bilddatei (`*.png`) eine Datei mit dem PlantUML-Quelltext (`*.plantuml`) hinterlegt. Für das folgende Bild findet sich der Quelltext [beispielsweise hier](https://oer-informatik.gitlab.io/uml/umlklasse/plantuml/01_Klassendiagramm-ohneFormat.plantuml).

Ich mag die bunten Symbole nicht, die einige UML-Tools in Klassendiagramme setzen. Sie sind nicht UML-konform  und nicht selbsterklärend: Kreise, Rechtecke, Rauten.

Daher empfehle ich, diese mit dem `hide circle` und `skinparam`-Befehl zu deaktivieren. Ein PlantUML-Skript für Klassendiagramme sollte also in jedem Fall folgenden Aufbau haben:

```plantuml
@startuml
hide circle
skinparam classAttributeIconSize 0

' Hier werden die eigentlichen UML-Deklarationen ergänzt.
' Kommentare können wie hier hinter dem Tickzeichen angefügt werden.

@enduml
```

![Anpassungen - Darstellung ohne Symbole](plantuml/01_Klassendiagramm-HideCircle.png)

### Eine Klasse mit Member notieren

In der einfachsten Form besteht eine Klasse nur aus dem Klassennamen. In PlantUML notiert man:

```plantuml
class Konto{
}
```

![Ein einfaches UML-Klassendiagramm mit PlantUML erstellt](plantuml/02_Klasse.png)

Die Klasse kann um Member (Attribute und Methoden) ergänzt werden. PlantUML erkennt dabei selbständig, ob es sich um eine Methode handelt (anhand der Klammer). Klassenattribute und -methoden werden mit `{static}` notiert. Die Deklaration hat den folgenden Aufbau:

* Instanz-Attribute: `attributname: Datentyp`
(konkret: `iban: String`)

* Instanz-Methoden: `methodenname(Parametername: Datentyp): RückgabeDatentyp`
(konkret: `auszahlen(betrag:double):boolean`)

* Klassenattribute: `{static} klassenAttribut: Datentyp`
(konkret: `{static} kontenListe: Konto[0..*]`)

* Klassenmethoden: `{static} klassenMethode(...): RückgabeDatentyp`
(konkret: `{static} listeStatusAllerKonten():String`)

Eine einfache Kontoklasse würde mit PlantUML folgendermaßen notiert:

```plantuml
class Konto{
	iban: String
	kontostand: double
	{static} kontenListe: Konto[0..*]
	Konto(iban: String)
	einzahlen(betrag:double):void
	auszahlen(betrag:double):boolean
	getKontostand():double
	kontoStatus():String
	{static} listeStatusAllerKonten():String
}
```

![Die Klasse Konto mit Instanz-Attributen und -Methoden sowie statischen Membern](plantuml/01_Klassendiagramm.png)

### Sichtbarkeitsmodifizierer

In PlantUML werden die Sichtbarkeitsmodifizierer in gleicher Weise und mit identischer Symbolik den Membern der Klasse vorangestellt wie in der UML.

* _public_ / sichtbar: `+` (im Diagramm unten z.B. `+getIban():String`),

* _private_ / versteckt: `-` (das gekapselte Attribut `-iban: String`),

* _protected_ / klassensichtbar: `#` (das Attribut `#kontostand: double`),

* _package_ / paketsichtbar: `~` (der Konstruktor: `~Konto(iban:String)`)

Wichtig ist, dass im Kopf des PlantUML-Quelltexts die Option `skinparam classAttributeIconSize 0` steht, da sonst andere Symbole (Quadrat statt `-`, Raute statt  `#`, Kreis statt  `+`, Dreieck statt `~`) für die Sichtbarkeitsmodifizierer angezeigt werden.

In unserem Beispiel:

```plantuml
class Konto{
	-iban: String
	#kontostand: double
	+getIban():String
	+setIban(iban:String):void
	~Konto(iban:String)
	}
```

![Die Klasse Konto mit gekapseltem Attribut, paketsichtbarem Konstruktor und klassensichtbarem `kontostand`](plantuml/01_Klassendiagramm-alleSichtbarkeitsmodifizierer.png)

### Vererbung

Vererbungsbeziehungen werden in PlantUML mit stilisierten Pfeilen mit geschlossener Pfeilspitze notiert: `-|>`

Sie können ohne Richtungsangabe (_default_: nach rechts) oder gezielt per `up`, `down`, `left`, `right` ausgerichtet werden:



```plantuml
Investment <|- Konto
PrivatKonto -left-|> Konto
GeschaeftsKonto -up-|> Konto
VereinsKonto -down-|> Konto
PrivatKonto <|-right- SparKonto
```

![Vererbungsbeziehungen in alle Richtungen](plantuml/03_Klassendiagramm-Vererbung-inalleRichtungen.png)

### Assoziation

Assoziationen werden als Linie zwischen zwei Klassen dargestellt. In PlantUML werden diese durch zwei Bindestriche notiert:

```plantuml
Kunde -- Rechnung
```

![Assoziation zwischen Kunde und Rechnung](plantuml/05-Klassendiagramm-Assoziation.png)

Assoziationen können mit Namen und Leserichtungen versehen werden. Der Name wird nach dem Doppelpunkt angegeben, die Leserichtung als stilisierter Pfeil vor (`<`) oder nach (`>`) dem Namen.

```plantuml
Kunde -- Rechnung: bezahlt >
```

```plantuml
Kunde -- Rechnung: < wird bezahlt von
```

![Assoziation: ein Kunde bezahlt eine Rechnung](plantuml/05-Klassendiagramm-Assoziation-Name.png)

![Assoziation in Gegenrichtung: eine Rechnung wird von einem Kunden bezahlt](plantuml/05-Klassendiagramm-Assoziation-Name2.png)

#### Multiplizitäten

Multiplizitäten werden an den Enden in Anführungszeichen angegeben: `Mitarbeiter "1..*" -- "0..*" Bank`

Mitunter kann es sinnvoll sein, einen _leeren Assoziationsnamen_ zu vergeben, da sonst die Klassen von PlantUML so eng nebeneinander gezeichnet werden, dass eine Zuordnung der Multiplititäten schwierig ist (siehe hinter Räder:"..."):

```plantuml
Fahrzeug "1"-"1..*" Räder: "                 "
```

![Multiplizitäten im Klassendiagramm](plantuml/05-Klassendiagramm-Assoziation-Multiplizität-Kardinalität.png)

Assoziationen lassen sich - analog zu den Vererbungen und ebenso wie alle Kanten im UML-Klassendiagramm - in jede Richtung legen:

```plantuml
Rechnung "*"--right--"1" Lieferanschrift: "        "
Rechnung "1"--left--"1..*" Rechnungsposition: "        "
Rechnung "*"-up-"0..1" Rechnungsanschrift
Rechnung "*"-down-"1..2" Steuersatz
```

![Ausrichtung von Assoziationen](plantuml/05-Klassendiagramm-Assoziation-Multiplizitäten.png)

#### Navigierbarkeit von Assoziationen

Wenn nicht durch ein offenes Kantenende unbestimmt bleiben soll, in welche Richtung eine Assoziation navigiert werden kann, so kann die Richtung in PlantUML per stilisiertem Pfeil `>` explizit erlaubt oder per stilisiertem Kreuz `x` explizit verboten werden:

```plantuml
Kunde x-> Rechnung : bezahlt >
```
![Navigation explizit erlaubt (>) bzw. verboten (x)](plantuml/05-Klassendiagramm-Assoziation-Navigationsrichtung.png)

#### Komposition und Aggregation

Die engeren Objektbeziehungen Aggregation ("ist Teil von") und Komposition ("ist existenzabhängiges Teil von") werden ebenso an den Kantenenden notiert:

Bei einer Aggregation wird ein kleines `o` auf der Seite des Ganzen eingefügt:

```plantuml
Kunde o- Konto
```

![Aggregationen werden durch "o" dargestellt](plantuml/05-Klassendiagramm-Aggregation-klein.png)

Bei einer Komposition ersetzt ein Asterisk (`*`) die ausgefüllte Raute aufseiten des Ganzen.

```plantuml
Rechnung *- Rechnungsposition
```

![Komposition wird mit Asterisk als Raute dargestellt](plantuml/05-Klassendiagramm-Komposition-klein.png)

Diese Elemente lassen sich natürlich auch mit Assoziationsnamen, Leserichtung, Multiplizitäten usw. verbinden:

![Verschiedene Objekt- und Klassenbeziehungen](plantuml/05-Klassendiagramm-Objekt-und-Klassenbeziehungen.png)

### Abstrakte Methoden, abstrakte Klassen und Interfaces

Die Notation von Interfaces und abstrakter Klassen erfolgt analog der bisher dargestellten Logik:

#### Interfaces

Interfaces werden zwar mit dem Schlüsselwort `Interface` in PlantUML bereits als solche gekennzeichnet, jedoch sieht man diese Kennzeichnung nur, wenn der `hide circle`-Befehl nicht genutzt wird (den ich immer aktiv habe). Daher muss zusätzlich das Stereotyp `<<Interface>>` in Guillemets (doppelte spitze Klammern) angegeben werden. Der sonstige Aufbau entspricht dem der Klassen:

```plantuml
interface Verzinsbar <<Interface>>{
  {abstract} +verzinse()
}
```

![UML Klassendiagramm mit Interface Verzinsbar, kursiv und mit Guillements markiert](plantuml/06-Klassendiagramm-Interfaces-Implementierung-Interface.png)

Die Implementierungspfeilspitze ist in der UML die gleiche wie bei der Vererbung. Entsprechend muss in PlantUML nur der Linientyp angepasst werden (gestrichelt), in dem statt der Bindestriche Punkte angegeben werden.

```plantuml
Verzinsbar <|.. SparKonto
```

![Der Implementierungspfeil zeigt von der Klasse SparKonto auf das Interface Verzinsbar (nicht ausgefülltes Dreieck, gestrichelte Linie)](plantuml/06-Klassendiagramm-Interfaces-Implementierung.png)

Abstrakte Methoden (also Methodenköpfe ohne Implementierung) lassen sich in PlantUML über das Schlüsselwort `{abstract}` definieren (in geschweiften Klammern als Constraint formuliert). Das `{abstract}` kann vor oder hinter dem Member stehen. Diese werden dann UML-konform (wie oben) kursiv dargestellt.

```plantuml
  {abstract} verzinse()
```

Damit der Constraint `abstract` selbst erscheint (das ist bei Interfaces in der Regel weniger wichtig als bei abstrakten Klassen), muss ein kleiner Kunstgriff angewendet werden und ein weiteres `{abstract}` im ersten kaskadiert werden:

```plantuml
  verzinse() {{abstract} abstract}
```

#### Abstrakte Klassen

Klassen, die nicht instanziiert werden können (weil sie abstrakte Methoden enthalten) oder nicht instanziiert werden sollen, werden ebenso mit dem Schlüsselwort `abstract` gekennzeichnet, hier aber nicht als Constraint, also ohne Klammern:

```plantuml
abstract class Geldanlage{
  +getAnlagewert(): double {abstract} 
}
```

PlantUML stellt abstrakte Strukturen dann _kursiv_ dar:

![Eine abstrakte Klasse mit einer abstrakten Methode](plantuml/06-Klassendiagramm-abstrakte-Klasse.png)

Da die kursive Schreibweise leicht übersehen werden kann, ist zu empfehlen, zusätzlich den _constraint_ `{abstract}` in den Klassenkopf zu schreiben. Man muss hierzu bei PlantUML tricksen: Der Name wird um den _constraint_ erweitert und die Klasse fortan über den Alias angesprochen:

```plantuml
abstract class "Geldanlage\n{abstract}" as Geldanlage{
  +getAnlagewert(): double {{abstract}abstract} 
}
```

![explizit per _constraint_ als abstrakt gekennzeichnete Klasse](plantuml/06-Klassendiagramm-abstrakte-Klasse-explizit-notiert.png)

#### Interfaces in Lollipop-Notation

Bei loser Kopplung wird die Objektbeziehung nicht über eine konkrete Referenz (also eine konkrete Klasse) hergestellt, sondern über eine Abstraktion - in der Regel ein Interface. Im UML-Diagramm lässt sich das wie folgt darstellen:

![Die Implementierung und Nutzung des Interfaces in Standard-Notation](plantuml/06-Klassendiagramm-Interfaces-Implementierung-Detail.png)


```plantuml
interface Buchbar <<Interface>>{
  +{abstract} abbuchen(betrag:double): void
  +{abstract} einzahlen(betrag: double): void
}

class Konto{...}

class Kunde{...}

Kunde .down.> Buchbar : <<use>>
Buchbar  <|.down. Konto : "     "
```

_Kunde_ nutzt ein Objekt vom Typ _Buchbar_, um einen Artikel zu bezahlen (siehe Auszug aus der Methode `bezahlen()`).

Wenn der Aufbau des Interfaces nicht so wichtig ist, wird häufig die _Lollipop_-Notation gewählt, die nur die Schnittstelle benennt, die _Member_ des Interface aber nicht zeigt.

![Die Implementierung und Nutzung des Interfaces in Standard-Notation](plantuml/06-Klassendiagramm-Interfaces-Implementierung-Lollipop.png)

Damit PlantUML das Interface in Lollipop-Notation darstellt, muss das Interface selbst mit dem Schlüsselwort `circle` gekennzeichnet werden (anstelle `interface` im Beispiel oben). Die Abhängigkeit der Interface-Nutzung wird als stilisierte Buchse dargestellt: `-(`.

```plantuml
class Konto
class Kunde
circle Buchbar


Kunde -( Buchbar
Buchbar - Konto
```

Die nutzende und die implementierende Klasse können auch jeweils einzeln mit dem Interface dargestellt werden. Die implementierende Klasse bietet die Schnittstelle an, was als "Stecker" / Lollipop symbolisiert wird `-()`. Die nutzende Klasse bietet die passende Buchse `-(`. Das Interface selbst muss wieder als `circle` definiert werden.


```plantuml
circle Buchbar

Kunde -( Buchbar

Konto -() Buchbar
```

![Die Lollipop-Darstellung mit einzelner Buchse und Stecker](plantuml/06-Klassendiagramm-Interfaces-Implementierung-Lollipop-einzeln.png)

Leider stellt PlantUML auch bei der Klasse, die das Interface nutzt (hier: Kunde) den Kreis dar. Im UML-Standard würde hier nur die Buchse (_socket_) dargestellt und das Interface benannt. Eine wirklich minimale Abweichung von der Standard-Notation.

### Aufzähungen: ENUMS

Aufzählungen werden in PlantUML als Klasse umgesetzt, die mit dem Stereotyp `<<enumeration>>` versehen werden:

```plantuml
class   Wochentage <<enumeration>>{
MONTAG
DIENSTAG
MITTWOCH
DONNERSTAG
FREITAG
SAMSTAG
SONNTAG
}
```

![Bei ENUMS handelt es sich in plantUML um einfache mit Stereotyp versehene Klassen](plantuml/07-Klassendiagramm-Enums.png)


### Generics

Mit PlantUML lassen sich auch parametrisierte Klassen darstellen:

```plantuml
class Optional<T> {
}
```

Die Typvariable wird in einfache spitze Klammern eingetragen (`<T>`) und in der Klasse als Datentyp weiterverwendet.

![Einfache Generics in PlantUML](plantuml/08-Klassendiagramm-Generics.png)

Für den Typparameter können - wie in vielen Programmiersprachen üblich - auch Grenzen angegeben werden:

```plantuml
class Foo<? extends Element> {
  int size()
}
Foo *- Element
```

![Generic mit Boundaries](plantuml/08-Klassendiagramm-Generics-Boundaries.png)

### Notizen anordnen

In PlantUML können Notizen in allen vier Richtungen um eine Klasse herum angeordnet werden:

![Notizen zu einer Klasse auf allen Seiten](plantuml/08-Klassendiagramm-Notizen-gesamt.png)

Wenn mehrere Notizen in die gleiche Richtung gehen, werden sie leider etwas unpassend angeordnet. Der PlantUML-Quelltext sieht folgendermaßen aus: 

```plantuml

class Konto{	}

note top of Konto
	oberhalb
end note

note bottom of Konto
 unterhalb
end note

note right of Konto
 nochmal rechts
end note

note right of Konto
 rechts
end note


note left of Konto
 links
end note
```

Die Notiz kann auch direkt auf einen Member zeigen.

![Notiz die direkt auf Member zeigt](plantuml/08-Klassendiagramm-Notizen-Member.png)

```plantuml
class Konto{
	-iban: String
	~Konto(iban:String)
	}

note left of Konto::iban
	"-": <i><b>private</b></i>
	Member nur innerhalb der Instanz zugreifbar
end note

note right of Konto::Konto
	"\~": <i><b>package</b></i> 
	Member nur innerhalb Instanzen der Klassen
	dieses Packages zugreifbar
end note
```

Eine Notiz kann auch direkt auf mehrere Member zeigen, dann werden Notiz und Member wie Assoziationen/Dependencies verknüpft.

![Notiz die direkt auf mehrere Member zeigt](plantuml/08-Klassendiagramm-Notizen-MehrereMember.png)

```plantuml
note as n_abstr
Für diese Methoden
gilt diese Notiz
end note

note as n_prot
Für diese Attribute gilt
diese Notiz
end note

note top of Konto
Diese Notiz gilt für die ganze Klasse
end note

n_abstr ..left.. Konto::getIban
n_abstr ..left.. Konto::setIban

n_prot ..right.. Konto::iban
n_prot ..right.. Konto::kontostand
```

Notizen können auch auf Assoziationen und andere Links positioniert werden - hier ist 

![Positionierung von Notizen bei PlantUML](plantuml/08-Klassendiagramm-Notizen-Links.png)

```plantuml
class Rechnung{}

Rechnung - Adresse: "ist Teil von "
note top on link
 top on
end note


Adresse - Kunde: "ist Teil von "
note bottom on link
 bottom on
end note


Kunde -Projekt: "ist Teil von "
note left on link
 left on
end note

Projekt - Abteilung: "ist Teil von "
note right on link
 right on
end note

Rechnung "*"-down-"1..2" Steuersatz: "ist Teil von "
note left on link
 left on mit Multiplizitäten
end note


Steuersatz "*"-right-"1..2" Abrechnung: "ist Teil von "
note right on link
 right on mit Multiplizitäten
end note


Abrechnung "*"-right-"1..2" Service: "ist Teil von " >
note top on link
 top on mit Leserichtung
end note

Service -right- Handler: "ist Teil von " >
note bottom on link
 bottom on mit Leserichtung
 ohne Multiplizität
end note
```

### Anordnung von Klassen und Umgruppierungen

PlantUML ordnet alle Klassen, Interfaces und Notizen in einem Grid an. Wenn man sich überlegt, an welcher Position eines regelmäßigen Grids eine Klasse stehen soll, so gelingt es gut, auf die Positionierung Einfluss zu nehmen.

![Positionierung im Grid](plantuml/09-Klassendiagramm-Positionierung-grid.png)

Man erkennt hier an dem Diagramm schön die regelmäßige Anordnung in einem 3x3 Raster - in der Mitte ist derzeit kein Element angeordnet.

- Man kann die Position über **die Richtung der Kante**, die diese Klasse mit einer anderen Klasse verbindet, festlegen (`-up-`, `-down-`, `-left-`, `-right-` bzw. `.up.`, `.down.`, `.left.`, `.right.`).

- Darüber hinaus kann man auch die Position über **die länge der Kanten festlegen** 
(`--` / `-up-` / `..` / `.up.` springt eine Position im Grid, `---` springt zwei Positionen `----` drei, usw.)

In folgendem Diagramm habe ich an den Kanten notiert, mit welchem Quelltext sie erzeugt wurden

![Positionierung im Grid](plantuml/09-Klassendiagramm-Positionierung-grid-pfeillaenge.png)

Auch hier lässt sich das Grid deutlich erkennen. Die unterschiedlichen Positionen wurde im Wesentichen über die Pfeillängen bestimmt:

```plantuml
Hund . Katze: " . "
Maus .left. Katze: " .left. "
Wolf <|-down- Hund: " <|-down- "
Maus .right. Hase: "  .right.  "
Kojote - Fuchs  : " - "
Fuchs --Waschbär: " -- "
Fuchs --- Dachs: " --- "
Fuchs ----Hyäne: " ---- "
Kojote ----left---- Hamster: "- -left--"
Kojote --down-- Hase:  "- - down --"  
Säugetier <|--down-- Wolf:  "- - down --"
Tigerkatze --down-|> Katze: " --down-|>"
```

#### Anordnung über versteckte Pfeile

PlantUML versucht die einzelnen Klassen möglichst kompakt und ohne überschneidende Kanten darzustellen. Oft will man aber eine bestimmte Gruppierung von Klassen erreichen. Da hierfür PlantUML nicht ausgelegt ist, muss man ein paar _schmutzige Tricks_ anwenden, um das gewünschte Layout zu erreichen:

Ausgeblendete Kanten: Wir können Klassen miteinander in Bezug bringen und diese Bezüge nicht anzeigen, in dem wir die Kanten mit `[hidden]` notieren. Diese Klassen werden so in der Nähe dargestellt:

```plantuml
Pferd -right[hidden]-> Hund
Hund -right[hidden]-> Katze
Katze -down[hidden]-> Maus
```

![Über unsichtbare Beziehungen positionieren](plantuml/09-Klassendiagramm-Positionierung-hidden.png)

#### Beziehungen bei Anordnung ignorieren

Wenn zwei Klassen zwar in einer Beziehung stehen, diese aber keinen Einfluss auf die Positionierung haben soll, so kann dies über `[norank]` dargestellt werden:

```plantuml
Pferd .right.> Hund
Hund .right.> Katze
Katze .down.> Maus
Maus .[norank].> Pferd
```

![Beziehungen nicht in Positionier-Logik einbeziehen](plantuml/09-Klassendiagramm-Positionierung-norank.png)

Ohne `[norank]` würde die Maus zum Pferd rücken, so bleibt sie aber am rechten Rand.

#### Anordnung über ausgeblendete Klassen

Über Ausblendungen lassen sich eine ganze Reihe Positionierungen umsetzen. Im folgenden Beispiel ist die obige Klasse Katze per `hide` ausgeblendet worden:

```plantuml
Pferd .right.> Hund
Hund .right.> Katze
Katze .down.> Maus
Maus .[norank].> Pferd

hide Katze
```

![Verstecken von Elementen mit `hide`](plantuml/09-Klassendiagramm-Positionierung-hide.png)

Denselben Effekt kann man erzielen, wenn man per Stereotyp ein transparentes Element erzeugt. Allerdings muss hier darauf geachtet werden, dass per `skinparam`-Befehl der Schatten und die Klassen-Kreise ausgeschaltet sind (was im Beispiel einmal bewusst nicht gemacht wurde - das korrekte Ergebnis sieht exakt wie oben aus!).

```plantuml
hide circle
skinparam shadowing false

skinparam class<<invisible>> {
  borderColor Transparent
  backgroundColor Transparent
  fontColor Transparent
  stereotypeFontColor Transparent
}

class Katze<<invisible>>{
}

Pferd .right.> Hund
Hund .right[hidden].> Katze
Katze .down[hidden].> Maus
Maus .[norank].> Pferd
```

![Verstecken von Elementen mit Stereotyp hier bewusst ohne `hide cicle ` und `skinparam shadowing false`](plantuml/09-Klassendiagramm-Positionierung-hide-stereotyp.png)

#### Gruppierung per `together {}`

Wenn Klassen zusammen stehen sollen, lässt sich das darüber hinaus über `together {...}` realisieren (Freizeichen vor der Klammer beachten!). Wieder am selben Beispiel demonstriert:


```plantuml
together {
	class Hund
	class Maus
}
together {
	class Pferd
	class Katze
}
Pferd .right.> Hund
Hund .right[hidden].> Katze
Katze .down[hidden].> Maus
Maus .[norank].> Pferd

```

![Gruppierung mit Hilfe von together](plantuml/09-Klassendiagramm-Positionierung-together.png)

### UML Objektdiagramm

Objekte werden in plantUML mit dem `object`-Keyword notiert. Allerdings werden die Instanznamen nicht von Haus aus unterstrichen (was aber zur Unterscheidung mit Klassen erforderlich ist), daher füge ich dies per Markdown- (fett) bzw. HTML- (unterstrichen) -Notation ein:

```plantuml
object "**<u>max:Kunde </u>**" as max {
}
```

Attributwerte werden in Slots zugewiesen:

```plantuml
object "**<u>festgeldkonto:Konto </u>**" as festgeldkonto{
	kontostand=5000
}
```

Instanziierung per _Dependency_ notiert:

```plantuml
max .left.> Kunde::Kunde: "<<instantiate>>"
```

Assoziationen zwischen den Klassen werden bei Objekten als Link dargestellt - der Attributname steht auf Seiten des Teils (nicht des Ganzen) notiert:

```plantuml
max --right--> "kontenliste[1]" festgeldkonto: "                                 "
```

Das ganze sieht dann (mit ein paar skinparam-Anpassungen) etwa so aus:

![Objektdiagramm mit Slots, Links und Instanziierung](plantuml/11-Objektdiagramm-BigPicture.png)

### Aufhübschen von UML-Klassendiagrammen

Über Skins bietet PlantUML die Möglichkeit, die Diagramme in Schriftart, Farben und Aussehen individuell anzupassen. An drei Beispielen soll hier kurz gezeigt werden, wie die Konfiguration mit `skinparam` das Aussehen der Klassendiagramme individualisieren kann.

##### Darstellung ohne zusätzliche `skinparam`-Befehle

Beispielhaft sie ein Klassendiagramm gänzlich ohne Anpassungen des Skins mit PlanUML folgendermaßen aus:

![Beispiel ohne Formatierung](plantuml/10-Klassendiagramm-aufhuebschen-ohne.png)

##### Skin mit angepassten Farben und näher am UML-Standard

Im folgenden Beispiel (das für die Diagramme oben verwendet wurde) wurden Farben und Schriftart anpasst, aber zusätzlich auch die oben genannten Tipps verwendet, um das Diagramm UML-konformer erscheinen zu lassen:

```plantuml
@startuml
hide circle
skinparam classAttributeIconSize 0

skinparam DefaultFontName "Lucida Sans Typewriter"

skinparam Class{
    BorderColor DarkSlateBlue
    BackgroundColor whitesmoke

}

skinparam Interface{
    BorderColor DarkSlateBlue
    BackgroundColor whitesmoke
}

skinparam Note{
    BorderColor DarkSlateBlue
    BackgroundColor LightYellow
}

skinparam ArrowColor DarkSlateBlue

@enduml
```

![Für diese Seite genutzte Formatierung](plantuml/10-Klassendiagramm-aufhuebschen-skin.png)

##### Klassendiagramme per Formatierung als _Entwurf_ kennzeichnen

Um den Entwurfscharakter eines Klassendiagramms zu unterstützen, kann ein Handschrift-Layout verwendet werden. Besonders wirkungsvoll ist das, wenn auch die Schrift authentisch handschriftlich aussieht. Dies ist z.B. bei der Schriftart "FG Virgil" der Fall. Welche Schriften das jeweilige System bietet lässt sich mit dem PlantUML-Befehl `listfonts` anzeigen. Wichtig ist: sofern eine Vektorgrafik ausgegeben wird (`svg`, `eps`) muss die Schrift auf allen anzeigenden Computern vorhanden sein, andernfalls werden Ersatzschriftarten verwendet.


```plantuml
skinparam style strictuml
skinparam DefaultFontName "FG Virgil"
skinparam handwritten true
skinparam monochrome true
skinparam packageStyle rect
skinparam shadowing false
```

![Über Skin und Schrift handschriftlich anmutende Darstellung mit PlantUML](plantuml/10-Klassendiagramm-aufhuebschen-handschrift.png)


##### Diagramme vereinheitlichen und Formatierung auslagern

Damit nicht alle PlantUML-Diagramme dieselben `skinparam`-Sequenzen am Beginn der PlantUML-Datei nennen müssen, kann eine zentrale Konfigurationsdatei eingebunden werden, die die Formatierungen enthält:

```plantuml
!includeurl https://PATH.TO/MY/UMLCLASS_CONFIG.cfg
```

Alle Befehle in dieser Datei werden ausgeführt, als stünden sie in der umgebenden PlantUML-Datei. Um ein einheitliches Layout zu erreichen ist diese Funktion sehr praktisch!

### Fazit

Der große Vorteil von PlantUML ist, dass Diagramme direkt in die Dokumentation eingebunden werden können und jederzeit änderbar sind. Diesen Vorteil erkauft man sich mit der etwas unflexibleren Positionierung. Mit etwas Übung und rechtzeitiger Toleranz bei suboptimaler Aufteilung der Klassen lassen sich Projekte so schnell dokumentieren. Bei großen Diagrammen besteht allerdings die Gefahr sich zu verzetteln, wenn man versucht, die Anordnung nach eigenen Vorstellungen zu beeinflussen.


### Links

Es finden sich zahlreiche Quellen und Dokumentationen zu PlantUML im Netz, erwähnenswert sind u.a.:

- [Zuvorderst natürlich die offizielle Dokumentation von PlantUML](https://plantuml.com/de/class-diagram)

- [Die PlantUML-Sprachreferenz als PDF](http://pdf.plantuml.net/PlantUML_Language_Reference_Guide_de.pdf)
