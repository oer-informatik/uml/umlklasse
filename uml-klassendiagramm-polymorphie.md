## Objektorientierung - das Konzept Polymorphie

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">hhttps://bildung.social/@oerinformatik/113407704530797657</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-klassendiagramm-polymorphie</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Ein zentrales Konzept der objektorientierten Programmierung (OOP) ist die Polymorphie: die Vielgestalt von Objekten. Es ermöglicht über Instanztypen das Verhalten eines Objekts festzulegen, während der Typ der Referenz festlegt, welche Member erreichbar sind. Objekte sind so flexibel über unterschiedliche Referenzen wiederverwertbar, ohne gecastet werden zu müssen._

Dieser Text ist ein Teil der Infotext-Serie zu UML-Klassendiagrammen:

* [Grundlagen UML-Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm)

* [Sichtbarkeitsmodifikatoren](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)

* [Objektbeziehungen (Assoziation/Aggregation/Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation)

* [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung)

* [abstrakte Klassen und Interfaces](https://oer-informatik.de/uml-klassendiagramm-interface-abstraktion)

* [UML-Klassendiagramm und Polymorphie](https://oer-informatik.de/uml-klassendiagramm-polymorphie)

* [Objektinstanzen im UML-Objektdiagramm darstellen](https://oer-informatik.de/uml-objektdiagramm)

* [UML-Klassendiagramme mit PlantUML erstellen](https://oer-informatik.de/uml-klassendiagramm-plantuml)


### Das Konzept der _Polymorphie_ (Vielgestalt) in der OOP

Bei der Vererbung handelt es sich um eine "ist-ein"-Beziehung zwischen erbenden Klassen. Ein `GeschaeftsKonto` ist in folgendem Beispiel ein `Konto`. Ein `SparKonto` ist ein `Konto`.

![In der Liste `kontenListe` werden alle Instanzen von Konto + Subklassen verwaltet](plantuml/03_Klassendiagramm-Vererbung-polymorph.png)

Die Vielgestalt (Polymorphie) der OOP greift diese Eigenschaft auf: Wenn ein `Geschäftskonto` ein `Konto` ist, dann kann ich es auch immer dann verwenden, wenn ich ein `Konto` verwenden kann. Ein `Geschäftskonto` kann also in unterschiedlicher Gestalt auftreten und genutzt werden: als `Konto` oder als `Geschäftskonto`.

Wir können also Objekte verwenden, ohne genau die Klasse zu kennen, aus der sie instanziiert wurden, solange wir eine ihrer Superklassen (oder Interfaces) kennen und nur deren Methoden verwenden.

Das Konzept lässt sich besser verstehen, wenn wir stark typisierte OOP-Sprachen (wie z.B. Java) betrachten, da hier zwischen dem deklarierten Referenztyp der Variablen und der Klasse der zugrundeliegenden Instanz unterschieden wird. Dynamisch typisierte Sprachen (wie z.B. Python) kennen das Konzept eines Referenztyps nicht. 

In stark typisierten Sprachen können wir Instanzen einer Subklasse mit Referenzen einer Superklasse ansprechen. Am Beispiel von Java sieht für unser obiges Beispiel etwa so aus:

```java
Konto konto1 = new Konto("DE123");
Konto konto2 = new PrivatKonto("DE456", "Max", "Mustermann");
Konto konto3 = new GeschaeftsKonto("DE789", "Muster GmbH", 400D);
Konto konto4 = new SparKonto("DE987", "Max", "Mustermann", 0.02D);
PrivatKonto konto5 = new SparKonto("DE654", "Max", "Mustermann", 0.01D);
```

Auch wenn die Referenz vom Typ der Superklasse ist: die aufgerufene Implementierung ist die der jeweiligen Unterklasse. Ein `SparKonto` kann nach außen auch die Gestalt eines `Konto` oder eines `PrivatKonto` annehmen - das ist die Vielgestalt, die _Polymorphie_.

In stark typisierten OOP-Sprachen wie Java hat das zur Folge, dass wir unterscheiden müssen, wessen Instanz ein Objekt ist und über welche Referenz es angesprochen wird. Wir erweitern unser Ausgangsbeispiel um ein Interface und fügen ein paar Details der Klassen an, um es plastischer zu machen:

![In der Liste `kontenListe` werden alle Instanzen von Konto + Subklassen verwaltet](plantuml/03_Klassendiagramm-Vererbung-polymorph-Details.png)

Mit diesem Wissen betrachten wir die folgenden Zeile genauer:

```java
Konto konto3 = new GeschaeftsKonto("DE789", "Muster GmbH", 400D);
konto3.auszahlen(500D);            //nutzt die Implementierung von Geschäftskonto 
konto3.setVerfuegungsrahmen(1000); //nicht möglich, da keine Methode von Konto

GeschaeftsKonto konto5 = new GeschaeftsKonto("DE789", "Muster GmbH", 400D);
konto5.auszahlen(500D);            //nutzt die Implementierung von Geschäftskonto 
konto5.setVerfuegungsrahmen(1000); //möglich, da Geschäftskonto-Referenztyp
```

Der Referenztyp von `konto3` ist `Konto`, es ist aber eine Instanz der Klasse `GeschaeftsKonto`. Wir glauben also, es mit einem `Konto` zu tun zu haben und können nur `Konto`-Methoden aufrufen. Der Aufruf `konto3.setVerfuegungsrahmen()` spricht keine Methode von `Konto` an und ist daher für `konto3` nicht verfügbar. 

Mit dem Namen `konto5` haben wir ein weiteres Geschäftskonto erzeugt, das wir diesmal über eine `Geschäftskonto`-Referenz ansprechen. `konto5.setVerfuegungsrahmen()` ist also aufrufbar.

**Der Referenztyp legt fest, welche Member der Klasse erreichbar sind.**

Der Instanztyp von `konto3` ist `GeschaeftsKonto`. Wenn wir `konto3.auszahlen()` aufrufen reagiert die Implementierung `auszahlen()`, die in `GeschaeftsKonto` festgelegt wurde. Dass `konto3` eine `Konto`-Referenz ist, spielt hier keine Rolle:

**Der Instanztyp legt fest, welche Implementierung bei Aufruf ausgeführt wird.**

Die gleichen Überlegungen können wir für Interfaces treffen:

```java
Zahlbar konto6 = new PrivatKonto("DE456", "Max", "Mustermann");
konto6.einzahlen(100); // nutzt die Implementierung, die Privatkonto von Konto geerbt hat
konto6.getName();      // nicht aufrufbar, da keinen Methode von Zahlbar
```
Der Referenztyp `Zahlbar` legt fest, dass wir nur die Methoden des Interfaces `Zahlbar` direkt aufrufen können (`einzahlen()`, `auszahlen()`). Der Instanztyp ist ein `Privatkonto`, es wird also die Implementierung genutzt, die in `Privatkonto` gilt. In diesem Fall wäre das die von `Konto` geerbte Implementierung, da sie nicht überschrieben wurden.

Das ermöglicht uns beispielsweise, alle Instanzen von `Konto` und deren Subklassen zentral zu verwalten, in dem die Superklasse eine Liste aller Konten als Klassenvariable (statisch) hält und verwaltet.


<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

Im Quelltext muss das neue Klassenattribut und die Klassenvariable in `Konto` eingefügt werden (sowie der Import für `java.util.ArrayList`):

```java
private static ArrayList<Konto> kontenListe = new ArrayList<>();

static String listeStatusAllerKonten(){
        String ausgabe ="";
        for (Konto einKonto: kontenListe){
            ausgabe = ausgabe + "\n" + einKonto.toString();
        }
        return ausgabe;
    }
```

Bereits so würde die polymorphe Struktur sichtbar, da die aufgerufene `toString()`-Methode (diese erben alle Java-Objekte von der Klasse `Object`) den Instanztyp ausgibt:

```
Alle Konten:
 - de.csbme.kontoverwaltung.Konto@5c8da962
 - de.csbme.kontoverwaltung.PrivatKonto@33e5ccce
 - de.csbme.kontoverwaltung.GeschaeftsKonto@5a42bbf4
 - de.csbme.kontoverwaltung.SparKonto@270421f5
```

Deutlich komfortabler wird die Ausgabe, wenn jede Klasse eine eigene Implementierung von `toString()` enthält, in der alle spezifischen Attributwerte ausgegeben werden (`getIban()` muss noch in `Konto` implementiert werden):

Für `Konto`:

```java
@Override
public String toString(){
    return "Konto "+iban+ " / Kontostand "+kontostand;
}
```

Für `PrivatKonto`:
```java
@Override
public String toString(){
    return "Privatkonto " + getIban()
    + " von " + getPrivatkundenName() + "/ Kontostand: "+kontostand;
}
```

Für `GeschäftsKonto`:
```java
@Override
public String toString(){
    return "Geschäftskonto " + getIban() + " von " + firmenname
    + "/ Kontostand: "+kontostand + " / Verfügungsrahmen: " + verfuegungsrahmen;
}
```

Für `SparKonto`:
```java
@Override
public String toString(){
    return "Sparkonto " + getIban()
    + " von " + getPrivatkundenName()
    + " / Kontostand: "+kontostand
    + " / Zins: " + String.format("%.2f", zinssatz*100) + "%";
}
```

Nach den Anpassungen ist der polymorphe Aufruf unterschiedlicher Instanztypen über eine `Konto`-Referenz in der Liste ebenso gut sichtbar, wie die Tatsache, dass jede Instanz die eigene Implementierung nutzt. Zudem sind die bei Aufruf über die `Konto`-Referenz zunächst unsichtbaren Methoden und Attribute für die jeweilige (interne) `toString()`-Methode weiterhin aufrufbar und Detailinformationen verfügbar:

```
Alle Konten:
 - Konto DE123 / Kontostand 500.0
 - Privatkonto DE456 von Max Mustermann/ Kontostand: 1000.0
 - Geschäftskonto DE789 von Muster GmbH/ Kontostand: 1601.0 / Verfügungsrahmen: 400.0
 - Sparkonto DE987 von Max Mustermann/ Kontostand: 102.0 / Zins: 2,00%
 ```
</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

Im Quelltext muss das neue Klassenattribut und die statische Methode in `Konto` eingefügt werden:

```python
class Konto:
    kontenliste = []

    def __init__(self, iban: str) -> None:
        #...
        Konto.kontenliste.append(self)
    
    @staticmethod
    def liste_status_aller_konten():
        ausgabe = "Alle Konten:\n"
        for konto in Konto.kontenliste:
            ausgabe += str(konto)+"\n"
        return ausgabe
```

Bereits so würde die polymorphe Struktur sichtbar, da die aufgerufene `str(konto)`-Methode den Instanztyp ausgibt:

```
Alle Konten:
<__main__.Konto object at 0x0000027E374953D0>
<__main__.Privatkonto object at 0x0000027E37495430>
<__main__.Geschaeftskonto object at 0x0000027E37495490>
<__main__.Sparkonto object at 0x0000027E374954F0>
```

Deutlich komfortabler wird die Ausgabe, wenn jede Klasse (wie im UML-Diagramm angegeben) eine eigene Implementierung von `__str__()` enthält (im UML-Diagramm angegeben ist die Java Entsprechung `toString()`). In dieser Implementierung von `__str__()` können alle spezifischen Attributwerte ausgegeben werden:

Für `Konto`:

```Python
class Konto:
    #...
    def __str__(self):
        return 'Der Kontostand von {0} ist {1}. (Objekt: {2})'.format(self.iban, self.kontostand, self.__repr__())
```

Für `PrivatKonto`:

```python
class Privatkonto(Konto):
    #...
    def __str__(self):
        return 'Privatkonto von '+self.__vorname + " " + self.__nachname+"\n"+super().__str__()
```

Für `GeschäftsKonto`:

```python
class Geschaeftskonto(Konto):
    #...
    def __str__(self):
        return 'Geschäftskonto von '+self.__firmenname+"\n"+super().__str__()
```

Für `SparKonto`:
```python
class Sparkonto(Privatkonto):
    #...
    def __str__(self):
        return 'Sparkonto mit einem Zins von '+str(self.__zinssatz)+"\n"+super().__str__()
```

Nach den Anpassungen ist der polymorphe Aufruf unterschiedlicher Instanztypen über die `Konto`-Liste ebenso gut sichtbar, wie die Tatsache, dass jede Instanz die eigene Implementierung nutzt. Zudem sind die bei Aufruf über die `Konto`-Liste zunächst unsichtbaren Methoden und Attribute für die jeweilige (interne) `__str__()`-Methode weiterhin aufrufbar und Detailinformationen verfügbar:

```
Alle Konten:
Der Kontostand von DE123 ist 100. (Objekt: <__main__.Konto object at 0x0000027E374953D0>)

Privatkonto von Martin Mustermann
Der Kontostand von DE345 ist 200. (Objekt: <__main__.Privatkonto object at 0x0000027E37495430>)

Geschäftskonto von Muster GmbH
Der Kontostand von DE345 ist 200. (Objekt: <__main__.Geschaeftskonto object at 0x0000027E37495490>)

Sparkonto mit einem Zins von 0.03
Privatkonto von Martin Mustermann
Der Kontostand von DE345 ist 206.0. (Objekt: <__main__.Sparkonto object at 0x0000027E374954F0>)
 ```

[Der Python Beispiel-Code findet sich unter diesem link (klick).](https://gitlab.com/oer-informatik/uml/umlklasse/-/raw/main/src/kontobeispiel_vererbung.py)

</span>
