
class Kunde:
    def __init__(self):
        self.__kontenliste = []

    def add_konto(self, konto):
        self.__kontenliste.append(konto)

    def gesamt_vermoegen(self):
        vermoegen = 0.0
        for konto in self.__kontenliste:
            vermoegen += konto.get_kontostand()
        return vermoegen
    

class Konto:
    def __init__(self, kontostand: float):
        self.__kontostand: float
        self.__kontostand = kontostand

    def get_kontostand(self):
        return self.__kontostand
    
    def set_bezahlt(self, bezahlt: bool, kunde):
        self.__bezahlt = bezahlt
        print("wurde gezahlt von "+str(kunde.get_name()))

privat_konto = Konto(100)
spar_konto = Konto(200)
giro_konto = Konto(300)

mein_kunde = Kunde()
mein_kunde.add_konto(privat_konto)
mein_kunde.add_konto(spar_konto)
mein_kunde.add_konto(giro_konto)

print('Gesamtvermoegen {0:0.2f} €'.format(mein_kunde.gesamt_vermoegen()))
