## Einstieg in objektorientierte Programmierung mit dem UML-Klassendiagramm

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-klassendiagramm</span>

> **tl/dr;** _(ca. 9 min Lesezeit): Das UML-Klassendiagramm ist die zentrale Modellierungssprache in der objektorientierten Programmierung (OOP). In diesem Einstiegsartikel geht es um den grundsätzlichen Aufbau einer Klasse und wie die Member einer Klasse (Attribute und Methoden) dargestellt werden. Das OOP-Konzept der Überladung und die Auszeichnung für Instanzattribute, Instanzmethoden, Klassenattribute und Klassenmethoden wird in diesem ersten Teil der Infotext-Serie zu UML-Klassendiagrammen vorgestellt._

Infotext-Serie zu UML-Klassendiagrammen:

* [Grundlagen UML-Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm)

* [Sichtbarkeitsmodifikatoren](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren)

* [Objektbeziehungen (Assoziation/Aggregation/Komposition)](https://oer-informatik.de/uml-klassendiagramm-assoziation)

* [Klassenbeziehungen (Vererbung)](https://oer-informatik.de/uml-klassendiagramm-vererbung)

* [abstrakte Klassen und Interfaces](https://oer-informatik.de/uml-klassendiagramm-interface-abstraktion)

* [Polymorphie in der objektorientierten Programmierung](https://oer-informatik.de/uml-klassendiagramm-polymorphie)

* [Objektinstanzen im UML-Objektdiagramm darstellen](https://oer-informatik.de/uml-objektdiagramm)

* [UML-Klassendiagramme mit PlantUML erstellen](https://oer-informatik.de/uml-klassendiagramm-plantuml)

Um komplexe Strukturen schneller erschließen zu können ist es hilfreich, sie auf wesentliche Kernaspekte zu reduzieren und grafisch darzustellen. In der _objektorientierten Programmierung_ (OOP) hat sich hierfür in Form des UML-Klassendiagramms ein Standard herausgearbeitet. Beim Entwurf, der Analyse, der Dokumentation und beim Testen kann in dieser Weise die Struktur von Softwarekomponenten in einem UML-Klassendiagramm erfasst und mit unterschiedlicher Detailtiefe dargestellt werden.

### Bestandteile einer Klasse

Klassen stellen Baupläne für Objekte dar. In diesen Bauplänen wird _Verhalten_ und _Zustand_ in einer gemeinsamen Struktur erfasst. Mithilfe dieser Baupläne werden Objekte erstellt als sind identifizierbare konkrete Ausprägungen (Instanzen) dieser Baupläne. Sie verfügen über gespeicherten Zustände (die _gleichen_ Attribute) sowie ein gemeinsames Verhalten (die _selben_ Methoden).
Objekte einer Klasse unterscheiden sich nur im Zustand (den Attributwerten), nicht im Verhalten.

Alle Konten haben gleiche Eigenschaften (z.B. einen Kontostand, eine IBAN) und dasselbe Verhalten (man kann auf sie einzahlen und von ihnen auszahlen lassen). Ein UML-Klassendiagramm, das beispielhaft _Konten_ modelliert, sieht wie folgt aus:

![Beispiel eines UML-Diagramms für eine Konto-Klasse](plantuml/01_Klassendiagramm.png)

In dieser Klasse sind vier unterschiedliche Bestandteile (_Member_) beschrieben: Attribute, Methoden, Klassenattribute und Klassenmethoden.

#### (Instanz-)Attribute

![in Klassendiagrammen stehen Attribute im mittleren Bereich](plantuml/01_Klassendiagramm-Attribute.png)

Instanzattribute beschreiben die Eigenschaften (den inneren Zustand) des jeweiligen konkreten Objekts. Wenn in der OOP von Attributen die Rede ist, werden in der Regel Instanzattribute gemeint. Jedes Objekt beeinflusst nur die eigenen Eigenschaften. Die Werte der Attribute können für jedes Objekt einer Klasse variieren. Im obigen Beispiel sind `iban` und `kontostand` Attribute von `Konto`.

Attribute werden im UML-Klassendiagramm in der Zelle unterhalb des Klassennamens notiert. Nach dem Namen kann per Doppelpunkt getrennt der Datentyp folgen.

Im Quelltext werden Attribute folgendermaßen deklariert:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
class Konto{
  String iban;
  double kontostand;
  ...
}
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Konto:
    def __init__(self):
        self.iban: str
        self.kontostand: float
        ...
```
</span>

#### Operationen (Methoden)

Operationen beschreiben das gemeinsame Verhalten aller Objekte einer Klasse. Sie nutzen oder verändern in der Regel die Attribute des jeweiligen Objekts.

![in Klassendiagrammen stehen Attribute im mittleren Bereich](plantuml/01_Klassendiagramm-Operationen.png)

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
class Konto{
  //... Code von oben
  void einzahlen(double betrag) {
    kontostand = kontostand + betrag;
  }
}
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Konto:
    # ... Code von oben
    def einzahlen(self, betrag:float):
        self.kontostand += betrag
```

</span>

#### Klassenattribute (statische Attribute)

![Klassenvariable werden im UML-Diagramm unterstrichen notiert.](plantuml/01_Klassendiagramm-statischeAttribute.png)

Klassenattribute haben für jedes Objekt der Klasse denselben Wert. Sie werden häufig mit dem Namen _statische Attribute_ bezeichnet.

Klassenattribute werden oft für Konstanten oder Arrays (Listen) aller Objekte einer Klasse genutzt. In Java werden Sie über das Schlüsselwort `static` deklariert, im UML-Diagramm durch Unterstreichung gekennzeichnet.

In diesem Beispiel verfügt die Klasse Konto über eine Liste, in der alle Konten gespeichert sind. Diese Liste ist eine Klasseneigenschaft, keine Objekteigenschaft.

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
class Konto{
  static ArrayList<Konto> kontenListe = new ArrayList<>();
  ...
}
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Konto:

    konten = []
    
    def __init__(self, iban: str) -> None:
        Konto.konten.append(self)
```

</span>

#### Klassenoperationen (statische Methoden):

![Klassenvariable werden im UML-Diagramm unterstrichen notiert.](plantuml/01_Klassendiagramm-statischeOperationen.png)

Klassenoperationen hängen nicht am Objektzustand, können also auf keine Instanzattribute zugreifen. Sie können nur auf Klassenvariablen zugreifen, benötigen aber zur Ausführung keinerlei Objektinstanz. Wie die Klassenattribute werden sie über das Schlüsselwort `static` deklariert und im UML-Diagramm durch Unterstreichung gekennzeichnet.

Die Klassenmethode `listeStatusAllerKonten()` gibt Informationen zu allen Instanzen aus, die in `kontenListe` gespeichert sind:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
class Konto{
  ...
  static String listeStatusAllerKonten(){
    String ausgabe ="";
    for (Konto einKonto: kontenListe){
        ausgabe = ausgabe + "\n" + einKonto.kontostatus();
    }
    return ausgabe;
}}
```


</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Konto:

    @staticmethod
    def liste_status_aller_konten():
        ausgabe = ""
        for konto in Konto.konten:
            ausgabe += konto.konto_status()
        return ausgabe
```

</span>

### Konstruktor

![Konstruktoren im UML-Klassendiagramm](plantuml/01_Klassendiagramm-Konstruktor.png)

Eine besondere Operation ist der Konstruktor. Hierbei handelt es sich um eine Methode, die immer aufgerufen wird, sobald ein neues Objekt erstellt wird. Die Methode heißt in vielen Programmiersprachen exakt wie die Klasse und kann als Argument Werte für verpflichtende Argumente entgegennehmen.

Konstruktoren werden im UML-Klassendiagramm mit dem Stereotyp `<<create>>` notiert. In vielen Programmiersprachen sind sie aber leicht daran zu erkennen, dass sie denselben Namen tragen, wie die Klasse selbst (Beispiel: `Konto()`). Daher wird die Markierung `<<create>>` nur dort verwendet, wo anhand des Namens nicht klar ist, dass es sich um einen Konstruktor handelt. 

Wird ein Objekt instanziiert, so erkennt man direkt an den Klammern, die der Klassenbezeichnung folgen, dass hier eine Methode (nämlich der Konstruktor) aufgerufen wird: 

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
Konto kt = new Konto();
```

Im obigen Beispiel erwartet der Konstruktor ein Argument für den Parameter `iban`:

```java
Konto hannesKonto = new Konto("DE123");
```

In Java werden Konstruktoren implementiert wie Methoden mit dem Namen der Klassen (großgeschrieben), die keinen Rückgabewert (auch nicht `void`) und daher auch kein `return`-Statement haben:

```java
Konto(String iban){
    this.iban = iban;
    kontenListe.add(this);
}
```
</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
kt = Konto()
```

Im obigen Beispiel erwartet der Konstruktor ein Argument für den Parameter `iban`:

```python
hannes_konto = Konto("DE123")
```

In Python werden Konstruktoren als Methoden mit dem Namen `__init__(self)` implementiert - wie andere _magic functions_ - mit dem doppelten Unterstrich vor und nach dem Namen. Wie bei den anderen Methoden einer Klasse wird als erster Parameter `self` angegeben - die Referenz auf die Instanz selbst (vergleichbar mit `this` in vielen anderen Programmiersprachen). Die `__init__()`-Methode hat keinen Rückgabewert.

```python
class Konto:
  
    def __init__(self, iban:str):   
      self.__iban = iban;
```

[Der Python Beispiel-Code findet sich unter diesem link (klick).](https://gitlab.com/oer-informatik/uml/umlklasse/-/raw/main/src/kunde_rechnung_unidirektional.py)

</span>

### Das OOP-Prinzip der Überladung

Häufig werden Konstruktoren auch _überladen_, um unterschiedliche Parameterkombinationen zur Objekterzeugung zu nutzen. Als `überladen` bezeichnet man Methoden, die einen identischen Namen, aber unterschiedliche Parameter haben - die somit über eine unterschiedliche Methodensignatur verfügen.

 Im Beispiel denkbar wäre, dass man ein Konto mit IBAN oder mit Kontonummer (`ktn`) und Bankleitzahl (`blz`) eröffnen kann.

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
Konto(String iban){
    this.iban = iban;
    kontenListe.add(this);
}

Konto(String ktn, String blz){
    this.iban = "DE__"+blz+ktn;
    kontenListe.add(this);
}
```

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
class Konto:
  
    def __init__(self, iban:str):   
      self.__iban = iban;

    def __init__(self, ktn:str, blz:str):   
      self.__iban = "DE__" +blz+ktn;
```

[Der Python Beispiel-Code findet sich unter diesem link (klick).](https://gitlab.com/oer-informatik/uml/umlklasse/-/raw/main/src/kunde_rechnung_unidirektional.py)

</span>

### Das ganze Beispiel als Java-Code

Wer versucht hat, das ganze per UML geplante OOP-Programm in Java nachzuvollziehen, sollte etwa diesen Quelltext am Ende vor sich haben und ausführen können: <button onclick="toggleAnswer('sourcecode')">Gesamtquelltext ein/ausblenden</button>

<span class="hidden-answer" id="sourcecode">

```java
package de.oerinformatik.crm;

import java.util.ArrayList;

public class Investmentrechner {
    public static void main(String[] args) {
        Konto hannesKonto = new Konto("DE123");
        Konto tobiasKonto = new Konto("456", "10010010");
        System.out.println(Konto.listeStatusAllerKonten());
    }
}

class Konto{
    String iban;
    double kontostand =0.0;
    static ArrayList<Konto> kontenListe = new ArrayList<>();

    void einzahlen(double betrag) {
      kontostand = kontostand + betrag;
    }

    boolean auszahlen(double betrag) {
        if (kontostand - betrag > 0){
            kontostand = kontostand - betrag;
            return true;
        }else{
            return false;
        }
    }
    
    static String listeStatusAllerKonten(){
        String ausgabe ="";
        for (Konto einKonto: kontenListe){
            ausgabe = ausgabe + "\n" + einKonto.kontostatus();
        }
        return ausgabe;
    }
    
    String kontostatus(){
        return "KontoNr "+this.iban+" Kontostand: " + Double.toString(this.kontostand);
    }
    
    Konto(String iban){
        this.iban = iban;
        kontenListe.add(this);
    }

    Konto(String ktn, String blz){
        this.iban = "DE__"+blz+ktn;
        kontenListe.add(this);
    }
  }
```
</span>

### Nächste Bestandteile des UML-Klassendiagramms:

In weiteren Artikel geht es um andere Eigenschaften des UML-Klassendiagramms, als nächstes wird auf [Kapselung, Sichtbarkeit und Getter und Setter eingegangen](https://oer-informatik.de/uml-klassendiagramm-sichtbarkeitsmodifikatoren).

### Links und weitere Informationen

- [Primärquelle: Klassendiagramme werden in Kapitel 11.4 der aktuellen Spezifikation von UML definiert: https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)
